package io.hbb.website;

import io.hbb.BO.Exception.LoginFail;
import io.hbb.BO.Exception.invalidMobileNumber;
import io.hbb.BO.POJO.JwtAuthenticationResponse;
import io.hbb.BO.POJO.NewPassword;
import io.hbb.BO.POJO.Sign;
import io.hbb.BO.UserProfileService;
import io.hbb.DAO.Exception.UpdateFail;
import io.hbb.DAO.POJO.User.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@RestController
public class AuthenticationService {

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationService.class);

   @RequestMapping(value="api/v1/auth/is_username_existed", method = RequestMethod.GET)
    @ResponseBody
    public boolean isUsernameExisted (@RequestParam("username") String name) {
        return UserProfileService.isUsernameExisted(name);
    }

    @RequestMapping(value="api/v1/auth/is_email_existed", method = RequestMethod.GET)
    @ResponseBody
    public boolean isEmailExisted (@RequestParam("email") String email) {
        return UserProfileService.isEmailExisted(email);
    }

    @RequestMapping(value="api/v1/auth/signup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> userSignup(
            @RequestBody Sign signup
    ){
        try {
                Profile user = new Profile(signup.getName(),signup.getPassword(),signup.getEmail(),
                        signup.getFamily_name(), signup.getGiven_name(), signup.getAddress(), signup.getBirthday());
            logger.info(user.toString());

            UserProfileService.Signup(user);
                return new ResponseEntity<>("Success", HttpStatus.OK);
        } catch(ParseException p) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Birthday should be in format \"yyyy-MM-dd\"");
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @RequestMapping(value="api/v1/auth/signin", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<JwtAuthenticationResponse> userSignin(
            @RequestBody Sign signin
    ){
        JwtAuthenticationResponse rs;
        try {
            rs = UserProfileService.Signin(signin);
        } catch (LoginFail e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (UpdateFail e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
        } catch (invalidMobileNumber e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(rs,HttpStatus.OK);
    }

    @RequestMapping(value="api/v1/auth/password", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> sendChangePasswordMail (
            @RequestBody Profile req
    ){
        if(!UserProfileService.isUsernameExisted(req.getName())) {
            // prevent phone number testing, always return Success
            return new ResponseEntity("Success",HttpStatus.OK);
        }

        try {
            UserProfileService.sendChangePasswordMail(req.getName(), req.getBirthday());
        } catch (Exception e) {
            logger.info(e.getMessage() );
        }

        return new ResponseEntity("Success",HttpStatus.OK);
    }

    @RequestMapping(value="api/v1/auth/password", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> changePassword (
            @RequestBody NewPassword req
    ){
        logger.info(req.getKey());
        logger.info(req.getPassword());
        UserProfileService.validateHashCodeAndChangePassword(req.getKey(), req.getPassword());
        return new ResponseEntity("Success",HttpStatus.OK);
    }
}
