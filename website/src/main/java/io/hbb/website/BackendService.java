package io.hbb.website;
import io.hbb.BO.AdvertisementService;
import io.hbb.DAO.Exception.UserHasActivated;
import io.hbb.DAO.Exception.invalidSMS;
import io.hbb.BO.Exception.waitSMSValidate;
import io.hbb.DAO.POJO.Advertisement.Advertisement;
import io.hbb.BO.POJO.PremiumResult;
import io.hbb.BO.POJO.SMSmessage;
import io.hbb.BO.UserProfileService;
import io.hbb.DAO.Exception.SendTooManySMS;
import io.hbb.DAO.POJO.Premium.AvailableCreditCard;
import io.hbb.DAO.POJO.Premium.City;
import io.hbb.DAO.POJO.Premium.CreditCard;
import io.hbb.BO.PremiumService;

import java.util.List;
import java.util.Map;


import io.hbb.DAO.POJO.User.Profile;
import io.hbb.BO.POJO.Sign;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class BackendService {
    private static Logger logger = LoggerFactory.getLogger(BackendService.class);

    @RequestMapping(value="api/v1/user/is_username_existed", method = RequestMethod.GET)
    @ResponseBody
    public boolean isUsernameExisted (@RequestParam("username") String name) {
        int userId =  ((Sign)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        return UserProfileService.isUsernameExistedExcludeMe(name, userId);
    }

    @RequestMapping(value="api/v1/user/is_email_existed", method = RequestMethod.GET)
    @ResponseBody
    public boolean isEmailExisted (@RequestParam("email") String email) {
        int userId =  ((Sign)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        return UserProfileService.isEmailExistedExcludeMe(email, userId);
    }

    @RequestMapping(value="api/v1/user/credit_card", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<CreditCard>> getCreditCard(){
        try {
            int userId =  ((Sign)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
            return new ResponseEntity<>(PremiumService.getCreditCard(userId),HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @RequestMapping(value="api/v1/user/credit_card", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<CreditCard>> setUserCreditCard(
            @RequestBody Map<String, List<Integer>> cards
    ){
        try {
            int userId =  ((Sign)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
            return new ResponseEntity<List<CreditCard>> (PremiumService.setUserCreditCard(userId,1,cards.get("cards")), HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @RequestMapping(value="api/v1/user/available_credit_card", method = RequestMethod.GET)
    @ResponseBody
    public  ResponseEntity<List<AvailableCreditCard>> getAvailaableCreditCard(){
        try {
            return new  ResponseEntity<>(PremiumService.getAvailableCreditCard(), HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @RequestMapping(value="api/v1/optimize", method = RequestMethod.GET)
    @ResponseBody
    public  ResponseEntity<List<PremiumResult>> optimizePremium(
            @RequestParam("budget") int budget,
            @RequestParam("shopping-date") String date,
            @RequestParam("city") int city
    ){
        int userId =  ((Sign)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        return new ResponseEntity<>(PremiumService.getPremium(userId, budget, date, city), HttpStatus.OK);
    }

    @RequestMapping(value="api/v1/city", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<City>> getCity ()
    {
        return new ResponseEntity<>(PremiumService.getCity(),HttpStatus.OK);
    }

    @RequestMapping(value="api/v1/user/profile", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Profile> getUserProfile () {
        Profile result = new Profile();
        int userId =  ((Sign)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        try{
             result = UserProfileService.getUserProfile(userId);
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @RequestMapping(value="api/v1/user/profile", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Profile> updateUserProfile (@RequestBody Profile profile) {
        Profile result = new Profile();
        int userId =  ((Sign)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        try{
            result = UserProfileService.updateUserProfile(userId, profile);
        } catch (waitSMSValidate w) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @RequestMapping(value="api/v1/user/sms", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<SMSmessage> smsSending (@RequestHeader Map<String, String> headers,  HttpServletRequest request ) {
        SMSmessage sms = new SMSmessage();
        sms.setMessage("SMS sand");

        String ip = headers.get("x-forwarded-for") == null ? request.getRemoteAddr() : headers.get("x-forwarded-for");
        int userId = ((Sign) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        try {
             UserProfileService.getAndSendSMS(userId, ip);
        } catch (SendTooManySMS e) {
            sms.setMessage("Send too many times");
        } catch (UserHasActivated e) {
            sms.setMessage("User has activated");
        }catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(null);
        }
        return new ResponseEntity<>(sms,HttpStatus.OK);
    }

    @RequestMapping(value="api/v1/user/sms", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> smsValidate (@RequestBody SMSmessage sms ) {
        int userId =  ((Sign)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();

        try{
            UserProfileService.validateSMS(userId, sms.getSms());
        } catch (invalidSMS e){
            return  ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(null);
        }
        return new ResponseEntity<>("Success",HttpStatus.OK);
    }

    @RequestMapping(value="api/v1/adv", method = RequestMethod.GET)
    @ResponseBody public ResponseEntity<List<Advertisement>> getAD() {
        List<Advertisement> result = AdvertisementService.getAD();
        return  new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value="api/v1/departments", method = RequestMethod.GET)
    @ResponseBody public ResponseEntity<Map<String, Map<String, Map<String,String>>> > getRandomDepartment() {
        Map<String, Map<String, Map<String,String>>>  result = AdvertisementService.getRandomDepartment();
        return  new ResponseEntity<>(result, HttpStatus.OK);
    }

 }
