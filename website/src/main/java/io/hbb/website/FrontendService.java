package io.hbb.website;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FrontendService {
    @RequestMapping("/")
    public String index() {
        return "index";
    }
}
