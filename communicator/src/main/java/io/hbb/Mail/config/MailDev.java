package io.hbb.Mail.config;

import io.hbb.Mail.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
@PropertySource("classpath:mailserver-dev.properties")
@Profile("dev")
public class MailDev extends ContentTemplate {
    private static Logger logger = LoggerFactory.getLogger(MailDev.class);

    @Autowired
    private Environment env;

    @Bean(name = "emailTemplate")
    public Properties getMailTemplate() throws Exception {
        return super.getTemplate("/mailTemplate.xml");
    }

    @Bean
    public EmailService getEmailService() {
        return new EmailService();
    }

    @Bean
    public JavaMailSender getJavaMailSender(Environment env) {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

        javaMailSender.setHost(env.getProperty("spring.mail.host"));
        javaMailSender.setPort(Integer.parseInt(env.getProperty("spring.mail.port")));

        return javaMailSender;

    }
}
