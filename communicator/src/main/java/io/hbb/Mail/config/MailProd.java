package io.hbb.Mail.config;

import io.hbb.Mail.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
@PropertySource("classpath:mailserver-prod.properties")
public class MailProd extends ContentTemplate {
    private static Logger logger = LoggerFactory.getLogger(MailProd.class);

    @Autowired
    private Environment env;

    @Bean(name = "mailTemplate")
    public Properties getMailTemplate() throws Exception {
        return super.getTemplate("/mailTemplate.xml");
    }

    @Bean
    public EmailService getEmailService() {
        return new EmailService();
    }

    @Bean
    public JavaMailSender getJavaMailSender(Environment env) {

        Properties mailProperties = new Properties();
        mailProperties.put("mail.smtp.auth", env.getProperty("spring.mail.properties.mail.smtp.auth"));
        mailProperties.put("mail.smtp.starttls.enable", env.getProperty("spring.mail.properties.mail.smtp.starttls"));
        mailProperties.put("mail.smtp.starttls.required", env.getProperty("spring.mail.properties.mail.smtp.starttls.required"));
        //mailProperties.put("mail.smtp.socketFactory.port", env.getProperty("spring.mail.properties.mail.smtp.socketFactory.port"));
        mailProperties.put("mail.smtp.debug", env.getProperty("spring.mail.properties.mail.smtp.debug"));
        mailProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        mailProperties.put("mail.smtp.socketFactory.fallback", env.getProperty("spring.mail.properties.mail.smtp.socketFactory.fallback"));

        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setJavaMailProperties(mailProperties);
        javaMailSender.setHost(env.getProperty("spring.mail.host"));
        javaMailSender.setPort(Integer.parseInt(env.getProperty("spring.mail.port")));
        javaMailSender.setUsername(env.getProperty("spring.mail.username"));
        javaMailSender.setPassword(env.getProperty("spring.mail.password"));
        return javaMailSender;

    }
}
