package io.hbb.Mail.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Properties;

public abstract class ContentTemplate {

    private static Logger logger = LoggerFactory.getLogger(ContentTemplate.class);


    protected Properties readProperties(String xmlFileName) throws Exception {
        Properties p = new Properties();
        InputStream is = ContentTemplate.class.getResourceAsStream(xmlFileName);
        p.loadFromXML(is);
        return p;
    }

    protected Properties getTemplate(String path) throws Exception {
        Properties p = new Properties();
        try {
            p = readProperties(path);
        } catch (Exception e) {
            throw e;
        }
        return p;
    }

}

