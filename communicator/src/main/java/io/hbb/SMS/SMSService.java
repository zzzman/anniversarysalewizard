package io.hbb.SMS;

import io.hbb.SMS.POJO.SMS;
import org.asynchttpclient.AsyncCompletionHandler;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.RequestBuilder;
import org.asynchttpclient.Response;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.Future;

public class SMSService {
    @Autowired
    private AsyncHttpClient ahc;
    private String url;
    private String id;
    private String pwd;

    public SMSService(String url, String id, String pwd) {
        this.url = url;
        this.id = id;
        this.pwd = pwd;
    }

    public Future<Response> sendSMS(SMS sms, AsyncCompletionHandler<Response> completionHandler ) {
        RequestBuilder builder=new RequestBuilder();
        builder.setUrl(url);
        builder.addQueryParam("id",id);
        builder.addQueryParam("password",pwd);
        builder.addQueryParam("tel", sms.getTo());
        builder.addQueryParam("msg",sms.getContent());
        builder.addQueryParam("mtype", "G");
        builder.addQueryParam("encoding", "utf8");

        return  ahc.executeRequest(builder.build(), completionHandler);

    }
}
