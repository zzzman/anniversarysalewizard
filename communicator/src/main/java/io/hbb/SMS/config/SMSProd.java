package io.hbb.SMS.config;

import io.hbb.Mail.config.ContentTemplate;
import io.hbb.SMS.SMSService;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClientConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.util.Properties;

@Configuration
@PropertySource("classpath:smsserver.properties")
public class SMSProd extends ContentTemplate {
    private static Logger logger = LoggerFactory.getLogger(SMSProd.class);

    @Autowired
    private Environment env;

    @Bean(name = "smsTemplate")
    public Properties getSMSTemplate() throws Exception {
        return super.getTemplate("/smsTemplate.xml");
    }

    @Bean
    public SMSService getSMSService () {
        String url = env.getProperty("spring.sms.url");
        String id = env.getProperty("spring.sms.id");
        String pwd = env.getProperty("spring.sms.password");
        return new SMSService(url, id, pwd);
    }

    @Bean
    public AsyncHttpClient getClient() {
        AsyncHttpClient asyncHttpClient = new DefaultAsyncHttpClient(new DefaultAsyncHttpClientConfig.Builder()
                .setConnectTimeout(10000)
                .setRequestTimeout(10000)
                .build());
        return asyncHttpClient;
    }
}
