package io.hbb.Mail;

import io.hbb.Mail.config.MailDev;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Properties;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MailDev.class)
@ActiveProfiles("dev")
public class configTests {
    @Autowired
    private EmailService emailService;

    @Autowired
    private Properties emailTemplate;

    @Test
    public void setEmailServiceIsNotNull() throws Exception {
        Assert.assertNotNull(emailService);
    }

    @Test
    public void setEmailTemplateIsNotNull() throws Exception {
        Assert.assertNotNull(emailTemplate);
    }
}
