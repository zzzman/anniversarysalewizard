package io.hbb.optimizer;

import java.util.List;

public class BoundedKnapsack extends ZeroOneKnapsack {
    public BoundedKnapsack() {}

    public BoundedKnapsack(int _maxBudget) {
        setMaxBudget(_maxBudget);
    }

    public BoundedKnapsack(List<Premium> _premiumList) {
        setPremiumList(_premiumList);
    }

    public BoundedKnapsack(List<Premium> _premiumList, int _maxBudget) {
        setPremiumList(_premiumList);
        setMaxBudget(_maxBudget);
    }

    @Override
    public List<Premium> calcSolution() {
        int n = PremiumList.size();

        // add items to the list, if bounding > 1
        for (int i = 0; i < n; i++) {
            Premium item = PremiumList.get(i);
            if (item.getBounding() > 1) {
                for (int j = 1; j < item.getBounding(); j++) {
                    add(item.getId(), item.getName(), item.getPrice(), item.getBonus());
                }
            }
        }

        super.calcSolution();

        // delete the added items, and increase the original items
        while (PremiumList.size() > n) {
            Premium lastItem = PremiumList.get(PremiumList.size() - 1);
            if (lastItem.getInKnapsack() == 1) {
                for (int i = 0; i < n; i++) {
                    Premium iH = PremiumList.get(i);
                    if (lastItem.getName().equals(iH.getName())) {
                        iH.setInKnapsack(1 + iH.getInKnapsack());
                        break;
                    }
                }
            }
            PremiumList.remove(PremiumList.size() - 1);
        }

        return PremiumList;
    }

    // add an item to the item list
    public void add(int id, String name, int weight, int value, int bounding) {
        if (name.equals(""))
            name = "" + (PremiumList.size() + 1);
        PremiumList.add(new Premium(id, name, weight, value, bounding));
        setInitialStateForCalculation();
    }
} // class