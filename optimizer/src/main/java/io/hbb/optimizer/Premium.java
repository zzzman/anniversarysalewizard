package io.hbb.optimizer;

public class Premium {
    protected int id;
    protected String name;
    protected int price;
    protected int bonus;
    protected int bounding = 1;
    protected BonusType type;


    protected int inKnapsack = 0; // the pieces of item in solution


    public Premium(Premium item) {
        setId(item.getId());
        setName(item.name);
        setPrice(item.price);
        setBonus(item.bonus);
        setBounding(item.bounding);
        setType(item.type);
    }

    public Premium(int _price, int _bonus) {
        setPrice(_price);
        setBonus(_bonus);
    }

    public Premium(int _price, int _bonus, int _bounding) {
        setPrice(_price);
        setBonus(_bonus);
        setBounding(_bounding);
    }

    public Premium(String _name, int _price, int _bonus) {
        setName(_name);
        setPrice(_price);
        setBonus(_bonus);
    }

    public Premium(int _id, String _name, int _price, int _bonus, int _bounding) {
        setId(_id);
        setName(_name);
        setPrice(_price);
        setBonus(_bonus);
        setBounding(_bounding);
    }

    public Premium(int _id, String _name, int _price, int _bonus) {
        setId(_id);
        setName(_name);
        setPrice(_price);
        setBonus(_bonus);
    }

    public Premium(String _name, int _price, int _bonus, int _bounding) {
        setName(_name);
        setPrice(_price);
        setBonus(_bonus);
        setBounding(_bounding);
    }
    public Premium(String _name, int _price, int _bonus, int _bounding, BonusType _type) {
        setName(_name);
        setPrice(_price);
        setBonus(_bonus);
        setBounding(_bounding);
        setType(_type);
    }

    public Premium(int _id, String _name, int _price, int _bonus, int _bounding, BonusType _type) {
        setId(_id);
        setName(_name);
        setPrice(_price);
        setBonus(_bonus);
        setBounding(_bounding);
        setType(_type);
    }

    public void setName(String _name) {name = _name;}
    public void setPrice(int _price) {price = Math.max(_price, 0);}
    public void setBonus(int _bonus) {bonus = Math.max(_bonus, 0);}
    public void setType(BonusType _type){type = _type;}
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public void setInKnapsack(int _inKnapsack) {
        inKnapsack = Math.min(getBounding(), Math.max(_inKnapsack, 0));
    }

    public void setBounding(int _bounding) {
        bounding = Math.max(_bounding, 0);
        if (bounding == 0)
            inKnapsack = 0;
    }

    public void checkMembers() {
        setId(id);
        setPrice(price);
        setBonus(bonus);
        setBounding(bounding);
        setInKnapsack(inKnapsack);
    }

    public String getName() {return name;}
    public int getPrice() {return price;}
    public int getBonus() {return bonus;}
    public int getInKnapsack() {return inKnapsack;}
    public int getBounding() {return bounding;}
    public BonusType getType(){return type;}

    public String toString() {
        return getName() + " [id = "+ getId() +"price = " + getPrice() + ", bonus = " + getBonus() + ", upperBound = "+ getBounding() + ", type = "+ getType().toString() +"]";
    }
}
