package io.hbb.optimizer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ZeroOneKnapsack {
    protected List<Premium> PremiumList  = new ArrayList<Premium>();
    protected int maxBudget        = 0;
    protected int solutionBudget   = 0;
    protected int profit           = 0;
    protected boolean calculated   = false;

    public ZeroOneKnapsack() {}

    public ZeroOneKnapsack(int _maxBudget) {
        setMaxBudget(_maxBudget);
    }

    public ZeroOneKnapsack(List<Premium> _PremiumList) {
        setPremiumList(_PremiumList);
    }

    public ZeroOneKnapsack(List<Premium> _PremiumList, int _maxBudget) {
        setPremiumList(_PremiumList);
        setMaxBudget(_maxBudget);
    }

    // calculte the solution of 0-1 knapsack problem with dynamic method:
    public List<Premium> calcSolution() {
        int n = PremiumList.size();

        setInitialStateForCalculation();
        if (n > 0  &&  maxBudget > 0) {
            List< List<Integer> > c = new ArrayList< List<Integer> >();
            List<Integer> curr = new ArrayList<Integer>();

            c.add(curr);
            for (int j = 0; j <= maxBudget; j++)
                curr.add(0);
            for (int i = 1; i <= n; i++) {
                List<Integer> prev = curr;
                c.add(curr = new ArrayList<Integer>());
                for (int j = 0; j <= maxBudget; j++) {
                    if (j > 0) {
                        int wH = PremiumList.get(i-1).getPrice();
                        curr.add(
                                (wH > j) ? prev.get(j) :
                                        Math.max(
                                                prev.get(j),
                                                PremiumList.get(i-1).getBonus() + prev.get(j-wH)
                                        )
                        );
                    } else {
                        curr.add(0);
                    }
                } // for (j...)
            } // for (i...)
            profit = curr.get(maxBudget);

            for (int i = n, j = maxBudget;  i > 0  &&  j >= 0; i--) {
                long tempI   = c.get(i).get(j);
                long tempI_1 = c.get(i-1).get(j);
                if (
                        (i == 0  &&  tempI > 0)
                                ||
                                (i > 0  &&  tempI != tempI_1)
                        )
                {
                    Premium iH = PremiumList.get(i-1);
                    int  wH = iH.getPrice();
                    iH.setInKnapsack(1);
                    j -= wH;
                    solutionBudget += wH;
                }
            } // for()
            calculated = true;
        } // if()
        return PremiumList;
    }

    // add an Premium to the Premium list
    public void add(int id, String name, int weight, int value) {
        if (name.equals(""))
            name = "" + (PremiumList.size() + 1);
        PremiumList.add(new Premium(id, name, weight, value));
        setInitialStateForCalculation();
    }

    // add an Premium to the Premium list
    public void add(int id, int weight, int value) {
        add(id,"", weight, value); // the name will be "PremiumList.size() + 1"!
    }

    // remove an Premium from the Premium list
    public void remove(String name) {
        for (Iterator<Premium> it = PremiumList.iterator(); it.hasNext(); ) {
            if (name.equals(it.next().getName())) {
                it.remove();
            }
        }
        setInitialStateForCalculation();
    }

    // remove all Premiums from the Premium list
    public void removeAllPremiums() {
        PremiumList.clear();
        setInitialStateForCalculation();
    }

    public double getProfit() {
        if (!calculated)
            calcSolution();
        return profit;
    }

    public long getSolutionBudget() {return solutionBudget;}
    public boolean isCalculated() {return calculated;}
    public long getMaxBudget() {return maxBudget;}

    public void setMaxBudget(int _maxBudget) {
        maxBudget = Math.max(_maxBudget, 0);
    }

    public void setPremiumList(List<Premium> _PremiumList) {
        if (_PremiumList != null) {
            PremiumList = _PremiumList;
            for (Premium Premium : _PremiumList) {
                Premium.checkMembers();
            }
        }
    }

    // set the member with name "inKnapsack" by all Premiums:
    private void setInKnapsackByAll(int inKnapsack) {
        for (Premium Premium : PremiumList)
            if (inKnapsack > 0)
                Premium.setInKnapsack(1);
            else
                Premium.setInKnapsack(0);
    }

    // set the data members of class in the state of starting the calculation:
    protected void setInitialStateForCalculation() {
        setInKnapsackByAll(0);
        calculated     = false;
        profit         = 0;
        solutionBudget = 0;
    }
} // class
