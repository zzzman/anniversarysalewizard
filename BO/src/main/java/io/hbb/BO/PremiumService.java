package io.hbb.BO;

import io.hbb.BO.POJO.PremiumResult;
import io.hbb.DAO.POJO.Premium.AvailableCreditCard;
import io.hbb.DAO.POJO.Premium.City;
import io.hbb.DAO.POJO.Premium.CreditCard;
import io.hbb.DAO.POJO.Premium.PremiumRaw;
import io.hbb.DAO.PremiumDAO;
import io.hbb.optimizer.BoundedKnapsack;
import io.hbb.optimizer.Premium;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class PremiumService {
    private static final Logger logger = LoggerFactory.getLogger(PremiumService.class);

    private static PremiumDAO premiumDAO;

    public static List<City> getCity() {
        List<City>result = new LinkedList<>();

        try {
            result = premiumDAO.getCities();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static List<PremiumResult> getPremium(int userId, int budget, String date, int city) {

        //need format check
        Date d = Date.valueOf(date);

        BoundedKnapsack bok = new BoundedKnapsack(budget/100);

        List<PremiumRaw> pr = new LinkedList<>();

        try {
            pr = premiumDAO.getPremiums(userId,d, city);
            for(PremiumRaw t : pr) {
                bok.add(t.getId(), t.getBank_name(), t.getPrice(), t.getBonus(), t.getBounding());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<PremiumResult> result = new LinkedList<>();

        for(Premium f :bok.calcSolution()) {
            if(f.getInKnapsack() !=0) {
                result.add(new PremiumResult(pr.get(f.getId()-1),f.getInKnapsack()));
            }
        }
        return result;
    }

    public static List<CreditCard> getCreditCard(int userId) throws Exception {
        return premiumDAO.getUserCreditCards(userId, 1);
    }

    public static List<AvailableCreditCard> getAvailableCreditCard() throws Exception {
        return premiumDAO.getAvailableCreditCards();
    }

    public static List<CreditCard> setUserCreditCard(int userId, int setId, List<Integer> cards) throws Exception {
        int[] tmp = cards.stream().mapToInt(Integer::intValue).toArray();
        premiumDAO.setUserCreditCards(userId, setId, tmp,10);
        return premiumDAO.getUserCreditCards(userId, setId);

    }
}
