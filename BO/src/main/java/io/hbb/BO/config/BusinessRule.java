package io.hbb.BO.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.util.Calendar;
import java.util.TimeZone;

@Configuration
@PropertySource("classpath:bo.properties")
public class BusinessRule {

    private static Logger logger = LoggerFactory.getLogger(BusinessRule.class);

    @Autowired
    private Environment env;

    public Environment getEnv() {
        return env;
    }

    @Bean
    public Calendar getCalendar() {
        Calendar c = Calendar.getInstance();

        String tzone = env.getProperty("bo.timezone") == null ? "Asia/Taipei" : env.getProperty("bo.timezone");
        c.setTimeZone(TimeZone.getTimeZone(tzone));
        return c;
    }

}
