package io.hbb.BO.Util;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Random;

import com.google.common.io.BaseEncoding;
import org.springframework.security.crypto.password.PasswordEncoder;

public class Secure implements PasswordEncoder {
    private static final Random RANDOM = new SecureRandom();


    public static String getPasswordHash(char[] password, byte[] salt, int iter) throws NoSuchAlgorithmException, InvalidKeySpecException {
        //int iterations = 250000;
        //String password = "password";
        //String salt = "salt";

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        //char[] passwordChars = password.toCharArray();
        KeySpec spec = new PBEKeySpec(password, salt, iter, 256);
        SecretKey key = factory.generateSecret(spec);

        byte[] passwordHash = key.getEncoded();
        return BaseEncoding.base64().encode(passwordHash);


        // SecretKey secret = new SecretKeySpec(key.getEncoded(), "AES");
    }

    public static byte[] getNextSalt() {
        byte[] salt = new byte[16];
        RANDOM.nextBytes(salt);
        return salt;
    }

    public static boolean isValidPassword(String storedPasswordHash, char[] password, byte[] salt, int iter) throws InvalidKeySpecException, NoSuchAlgorithmException {
        String p = getPasswordHash(password, salt, iter);
        return p.equals(storedPasswordHash);
    }

    public static String generateRendomHash() throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String tmp = RANDOM.nextLong()+"";
        MessageDigest digest = MessageDigest.getInstance("SHA-512");
        digest.reset();
        digest.update(tmp.getBytes("utf8"));
        return String.format("%0128x", new BigInteger(1, digest.digest()));
    }

    @Override
    public String encode(CharSequence charSequence) {
        return null;
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        return false;
    }

    @Override
    public boolean upgradeEncoding(String encodedPassword) {
        return false;
    }
}
