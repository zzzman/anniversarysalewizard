package io.hbb.BO.Util;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Properties;


public class Validator {
    private static Properties p;

    public void setProperties(Properties properties) {
        p = properties;
    }

    public static boolean isEmail(String address) {
        EmailValidator v= EmailValidator.getInstance();
        return v.isValid(address);
    }

    public static boolean isPhoneNumber(String num) {
       return num.matches("\\d{10}");
    }

}
