package io.hbb.BO;

import io.hbb.BO.Exception.*;
import io.hbb.BO.POJO.JwtAuthenticationResponse;
import io.hbb.BO.POJO.SMSmessage;
import io.hbb.BO.Util.Secure;
import io.hbb.BO.Util.Validator;
import io.hbb.BO.security.JwtTokenProvider;
import io.hbb.DAO.Exception.*;
import io.hbb.DAO.POJO.User.Profile;
import io.hbb.BO.POJO.Sign;
import io.hbb.DAO.UserProfileDAO;
import io.hbb.Mail.EmailService;
import io.hbb.Mail.POJO.Mail;
import io.hbb.SMS.POJO.SMS;
import io.hbb.SMS.SMSService;
import org.asynchttpclient.AsyncCompletionHandler;
import org.asynchttpclient.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.sql.rowset.serial.SerialBlob;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Date;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;


@Service
public class UserProfileService implements UserDetailsService {
    private static final Logger logger = LoggerFactory.getLogger(UserProfileService.class);
    private static UserProfileDAO userProfileDAO;
    private static PasswordEncoder passwordEncoder;
    private static AuthenticationManager authenticationManager;
    private static JwtTokenProvider tokenProvider ;

    @Autowired
    public void setJwtokenProvider(JwtTokenProvider j) {
        tokenProvider = j;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder pe) {
        passwordEncoder = pe;
    }

    @Autowired
    public void setAuthenticationManager(AuthenticationManager am) {
        authenticationManager = am;
    }


    private static SMSService smsService;
    private static Properties smsTemplate;

    private static EmailService emailService;
    private static Properties emailTemplate;


    @Autowired
    public void setSMSService(SMSService s) {
        smsService = s;
    }

    @Autowired
    @Qualifier("smsTemplate")
    public void setSMSTemplate(Properties p) {
        smsTemplate = p;
    }

    @Autowired
    public void setEmailService(EmailService e) {
        emailService = e;
    }

    @Autowired
    @Qualifier("mailTemplate")
    public void setEmailTemplate(Properties p) {
        emailTemplate = p;
    }

    public static Profile getUserProfile(int userId) {
        return userProfileDAO.getUserProfile(userId);
    }

    public static Profile updateUserProfile(int userId, Profile newProfile) throws Exception {
        Profile oldProfile =  userProfileDAO.getUserProfile(userId);
        boolean needUpdate = false;
        boolean needSMSValidate = false;

        if(!oldProfile.getEmail().equals(newProfile.getEmail()) ) {
            if (!Validator.isEmail(newProfile.getEmail())) {
                throw new invalidEmail(newProfile.getEmail() + " is invalid");
            }
        }

        if(!oldProfile.getName().equals(newProfile.getName())) {
            needUpdate = true;
            needSMSValidate = true;
            newProfile.setIs_activated(false);
            if(!Validator.isPhoneNumber(newProfile.getName())) {
                throw new invalidMobileNumber(newProfile.getName() + " is invalid");
            }
        } else {
           newProfile.setIs_activated(oldProfile.getIs_activated());
        }

        if(
                !oldProfile.getFamilyName().equals(newProfile.getFamilyName()) ||
                !oldProfile.getGivenName().equals(newProfile.getGivenName()) ||
                !oldProfile.getEmail().equals(newProfile.getEmail()) ||
                !oldProfile.getAddress().equals(newProfile.getAddress())
        ) {
            needUpdate = true;
        }

        if(newProfile.getPassword() != null) {
            needUpdate = true;
            String passwordHash = passwordEncoder.encode(newProfile.getPassword());
            newProfile.setPassword(passwordHash);
            //TODO:expire current JWT
        } else {
            newProfile.setPassword(oldProfile.getPassword());
        }

        if(needUpdate) {
            try {
                userProfileDAO.updateUserProfile(userId, newProfile);
            } catch (UpdateFail e) {
                throw new Exception(e.getMessage());
            }
        }

        if(needSMSValidate) {
            throw new waitSMSValidate("need sms validate due to update");
        }
        return userProfileDAO.getUserProfile(userId);
    }

    public static boolean isUsernameExistedExcludeMe(String username, int userId) {
        return UserProfileDAO.isUsernameExistedExcludeMe(username, userId);
    }

    public static boolean isEmailExistedExcludeMe(String email, int userId) {
        return UserProfileDAO.isEmailExistedExcludeMe(email, userId);
    }

    public static boolean isUsernameExisted(String username) {
        return UserProfileDAO.isUsernameExisted(username);
    }

    public static boolean isEmailExisted(String email) {
        return UserProfileDAO.isEmailExisted(email);
    }

    public static void Signup(Profile p) throws Exception {

        if(!Validator.isPhoneNumber(p.getName())) {
            throw new invalidMobileNumber(p.getName() + " is invalid");
        }

        if(!Validator.isEmail(p.getEmail())) {
            throw new invalidEmail(p.getEmail() + " is invalid");
        }

        byte[] salt = Secure.getNextSalt();
        try {
            p.setSalt(new SerialBlob(salt));
            String passwordHash = passwordEncoder.encode(p.getPassword());
            p.setPassword(passwordHash);
            userProfileDAO.userSignup(p);
        } catch(KeyDuplicated e) {
            throw new SignupFail("Name or email duplicated");
        } catch (Exception e) {
            logger.error("{} {}",e.getClass(), e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    public static JwtAuthenticationResponse Signin(Sign s)  throws LoginFail, UpdateFail, invalidMobileNumber {

        if(!Validator.isPhoneNumber(s.getName())) {
            throw new invalidMobileNumber(s.getName() + " is invalid");
        }

        Profile p = UserProfileDAO.getUserProfileViaName(s.getName());
        String jwt = "";
        if(passwordEncoder.matches(s.getPassword(), p.getPassword())) {
          //  logger.info((tokenProvider == null)+"");
           jwt = tokenProvider.generateToken(p);
           UserProfileDAO.updateLoginTime(p.getId());
        } else {
            throw new LoginFail("Login fail");
        }
        JwtAuthenticationResponse ret = new JwtAuthenticationResponse(jwt);
        ret.setGivenName(p.getGivenName());
        ret.setActivated(p.getIs_activated());

        return ret;
    }

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        Profile p = UserProfileDAO.getUserProfileViaName(name);
        Sign s = new Sign(p);
        s.grantAuthority();
        return s;
    }

    public UserDetails loadUserById(Long id) {
        Profile p = UserProfileDAO.getUserProfile((int) (long)id);
        Sign s = new Sign(p);
        s.grantAuthority();
        return s;
    }

    public static SMSmessage getAndSendSMS(int userId, String ip) throws UpdateFail, SendTooManySMS, UserHasActivated {
        SMSmessage ret = new SMSmessage();
        SecureRandom sr = new SecureRandom();
        int sms = Math.abs(sr.nextInt(1000000));
        ret.setSms("9"+String.format("%06d", sms));
        UserProfileDAO.setUserSMSCode(userId, ret.getSms(), ip);
        Profile p = UserProfileDAO.getUserProfile(userId);
        SMS smsSender = new SMS();
        smsSender.setContent(smsTemplate.getProperty("sms_confirm") + ret.getSms());
        smsSender.setTo(p.getName());

       // logger.info(smsSender.toString());

        Future<Response> future = smsService.sendSMS(smsSender, new AsyncCompletionHandler<Response>() {
            @Override
            public Response onCompleted(Response response) throws Exception {
                if(response.hasResponseStatus()){
                //    logger.info(response.getResponseBody());
                }
                return response;
            }

            @Override
            public void onThrowable(Throwable t) {
                super.onThrowable(t);
            }
        });

        try {
            Response response= future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return ret;
    }

    public static void validateSMS(int userId, String sms) throws invalidSMS, UpdateFail {
        UserProfileDAO.validateSMS(userId, sms);
    }

    public static void sendChangePasswordMail(String phone, Date birthday) throws BirthdayNotIdentity {
        Profile p = UserProfileDAO.getUserProfileViaName(phone);
        if(!p.getBirthday().toLocalDate().equals(birthday.toLocalDate())) {
           logger.info("id:{} expected birthday: {}, actual {}", p.getId(), p.getBirthday().toLocalDate(), birthday.toLocalDate() );
          throw new BirthdayNotIdentity();
        }

        String randomHash;
        try {
            randomHash = Secure.generateRendomHash();
        } catch (NoSuchAlgorithmException| UnsupportedEncodingException e) {
            logger.error(e.getMessage());
            throw new RuntimeException(e.getMessage());
        }

        UserProfileDAO.setChangePasswordHash(p.getId(), p.getEmail(), randomHash);

        String link = "https://www.card-u.me/change_pass.php?key="+randomHash;
        Mail mail = new Mail();
        mail.setFrom("support@card-u.me");
        mail.setTo(new String[]{p.getEmail()});
        mail.setSubject("Card-U系統密碼修改確認信件");
        mail.setContent(emailTemplate.getProperty("change_password").trim()+link);

        try {
            emailService.sendSimpleMessage(mail);
        } catch (Exception e) {
            logger.error("{} send to {}, Subject: {}, Content: {}"
                    , e.getMessage()
                    , p.getEmail()
                    , mail.getSubject()
                    , mail.getContent()
            );
        }
        logger.info("E");
        //TODO:delete this after  mail function ok
        logger.info("{} send to {}, Subject: {}, Content: {}"
                , ""
                , p.getEmail()
                , mail.getSubject()
                , mail.getContent()
        );

    }

    public static void validateHashCodeAndChangePassword(String hashCode, String password) {
        String passwordHash = passwordEncoder.encode(password);
        UserProfileDAO.updatePasswordViaHash(hashCode, passwordHash);
    }

}
