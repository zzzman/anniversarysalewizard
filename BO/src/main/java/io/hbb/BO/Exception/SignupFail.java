package io.hbb.BO.Exception;

public class SignupFail extends Exception {
    public SignupFail(String message) {
        super(message);
    }
}
