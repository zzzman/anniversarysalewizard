package io.hbb.BO.Exception;

public class LoginFail extends Exception {
    public LoginFail(String message) {
        super(message);
    }
}
