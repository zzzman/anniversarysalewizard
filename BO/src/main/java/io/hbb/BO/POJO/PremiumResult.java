package io.hbb.BO.POJO;

import io.hbb.DAO.POJO.Premium.PremiumRaw;

public class PremiumResult extends PremiumRaw {
    private int times;

    public PremiumResult(PremiumRaw pr , int times) {
        super.PremiumRaw(pr);
        this.times = times;
    }

    public int getTimes() {
        return times;
    }

    public void setTimes(int times) {
        this.times = times;
    }
}
