package io.hbb.BO.POJO;

import io.hbb.DAO.POJO.User.Profile;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.LinkedList;

import java.util.Objects;

public class Sign implements UserDetails {
    private int id;
    private String given_name;
    private String address;
    private String birthday;
    private boolean isEnabled;
    private String name;
    private String password;
    private String email;
    private String family_name;
    private Collection<? extends GrantedAuthority> authorities;

    public Sign(int id, String name, String email, String password,
                String address, String family_name, String given_name,
                String birthday,
                Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.authorities = authorities;
        this.address = address;
        this.family_name = family_name;
        this.given_name = given_name;
        this.birthday = birthday;
    }

    public Sign(Profile p) {
        this.id = p.getId();
        this.name = p.getName();
        this.email = p.getEmail();
        this.password = p.getPassword();
        this.address = p.getAddress();
        this.family_name = p.getFamilyName();
        this.given_name = p.getGivenName();
        this.birthday = p.getBirthday().toString();
        this.isEnabled = p.getIs_activated();
    }
    public String getFamily_name() {
        return family_name;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    public String getGiven_name() {
        return given_name;
    }

    public void setGiven_name(String given_name) {
        this.given_name = given_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIsEnabled(boolean enabled) {
       isEnabled = enabled;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getUsername() {
        return getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> a) {
        authorities = a;
    }

    public void grantAuthority() {
        Collection<SimpleGrantedAuthority> sgas = new LinkedList<>();
        sgas.add(new SimpleGrantedAuthority("ROLE_USER"));
        setAuthorities(sgas);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sign that = (Sign) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return String.format("id: %d, name:%s, password:%s, email:%s, family_name:%s, given_name:%s, address:%s, birthday:%s",
                id, name, password, email, family_name, given_name, address, birthday);
    }
}
