package io.hbb.BO;

import io.hbb.DAO.AdvertisementDAO;
import io.hbb.DAO.POJO.Advertisement.Advertisement;
import io.hbb.DAO.POJO.Advertisement.Department;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static io.hbb.DAO.AdvertisementDAO.getAdv;

@Service
public class AdvertisementService {
    private static Logger logger = LoggerFactory.getLogger(AdvertisementService.class);


    public static List<Advertisement> getAD() {
        List<Advertisement> ret = getAdv();
        return ret;
    }

    public static Map<String, Map<String, Map<String,String>>>  getRandomDepartment() {
        List<Department> ret =  AdvertisementDAO.getAllDepartment();
        //logger.info(ret.toString());
        Map<String, Map<String, Map<String,String>>> departmentTable= //new HashMap<>();
                ret.stream()
                        .collect(Collectors.groupingBy(Department::getArea,
                                Collectors.groupingBy(Department::getGroup,
                                        Collectors.toMap(e->e.getDepartment(), e->Optional.ofNullable(e.getUrl()).orElse(""))
                                )));

        return departmentTable;
    }
}
