package io.hbb.BO.security;

import io.hbb.DAO.POJO.User.Profile;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("dev")
public class JwtTokenProviderTest {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProviderTest.class);
    private JwtTokenProvider provider = new JwtTokenProvider("secrete", 1000* 24 * 60 * 60);
    private int id = 9527;


    @Test
    public void testGenerateTokenSuccess() {
        Profile p = new Profile();
        p.setId(id);
        String token = provider.generateToken(p);
        Assert.assertNotNull(token);
    }

    @Test
    public void testGetUserIdFromTokenSuccess() {
        Profile p = new Profile();
        p.setId(id);
        String token = provider.generateToken(p);
        long idFromToken = provider.getUserIdFromJWT(token);
        Assert.assertEquals(id, idFromToken);
    }

    @Test
    public void testValidateTokenFail() {
        String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI5NTI3IiwiaWF0IjoxNTYxMjc5MTg0LCJleHAiOjE1NjEzNjU1ODR9.oNfSGFGR9APqF1WTwZ019ykh3pY0SE0k0MHxYmAlkI5EL-wkz0c2Fn4R4aEFXhPpLqKY4rFiSFahmUoasoPJ";
        boolean res = provider.validateToken(token);
        Assert.assertFalse(res);
    }



}
