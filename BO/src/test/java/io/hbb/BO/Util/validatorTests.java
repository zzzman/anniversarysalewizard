package io.hbb.BO.Util;

import io.hbb.BO.config.BusinessRule;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Calendar;

@RunWith(SpringJUnit4ClassRunner.class)
public class validatorTests {

    @Test
    public void realPhoneNumber() throws Exception {
        Assert.assertTrue(Validator.isPhoneNumber("0961358196"));
    }

    @Test
    public void shortPhoneNumber() throws Exception {
        Assert.assertFalse(Validator.isPhoneNumber("096135819"));
    }

    @Test
    public void withAlphabetPhoneNumber() throws Exception {
        Assert.assertFalse(Validator.isPhoneNumber("O961358196"));
    }

    @Test
    public void withDashPhoneNumber() throws Exception {
        Assert.assertFalse(Validator.isPhoneNumber("-0961358196"));
    }

    @Test
    public void validEmail() throws Exception {
        Assert.assertTrue(Validator.isEmail("test@test.com"));
    }

    @Test
    public void withTopicDomainEmail() throws Exception {
        Assert.assertTrue(Validator.isEmail("test@tesT.com"));
    }

    @Test
    public void withTopicIdEmail() throws Exception {
        Assert.assertTrue(Validator.isEmail("Test@test.com"));
    }
}
