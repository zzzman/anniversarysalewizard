package io.hbb.BO;


import io.hbb.DAO.POJO.Premium.City;
import io.hbb.DAO.POJO.Premium.CreditCard;
import io.hbb.DAO.PremiumDAO;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ActiveProfiles;


import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.when;


@RunWith(PowerMockRunner.class)
@PrepareForTest({PremiumDAO.class})
@ActiveProfiles("dev")
public class premiumServiceTest {
    private static final Logger logger = LoggerFactory.getLogger(premiumServiceTest.class);

    @Before
    public void init() {


    }

    @Test
    public void testGetCitiesSuccess(){
        List<City>testCities = new LinkedList<>();
        testCities.add(new City(1, "台北"));
        testCities.add(new City(2, "桃園"));
        testCities.add(new City(3, "新竹"));
        testCities.add(new City(4, "高雄"));
        PowerMockito.mockStatic(PremiumDAO.class);
        when(PremiumDAO.getCities()).thenReturn( testCities);

        List<City> result = PremiumService.getCity();
        Assert.assertArrayEquals(testCities.toArray(), result.toArray());
    }

    @Test
    public void testGetCreditCardSuccess() {
        List<CreditCard> testCards = new LinkedList<>();
        testCards.add(new CreditCard(1,"國泰世華", "未指定"));
        testCards.add(new CreditCard(5,"富邦銀行", "未指定"));
        PowerMockito.mockStatic(PremiumDAO.class);
        when(PremiumDAO.getUserCreditCards(1,1)).thenReturn(testCards);
        List<CreditCard> result = new LinkedList<>();
        try {
            result = PremiumService.getCreditCard(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertArrayEquals(testCards.toArray(), result.toArray());
    }


}
