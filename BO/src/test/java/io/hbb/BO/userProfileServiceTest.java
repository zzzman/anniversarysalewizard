package io.hbb.BO;


import io.hbb.BO.Exception.waitSMSValidate;
import io.hbb.DAO.Exception.KeyDuplicated;
import io.hbb.DAO.Exception.UpdateFail;
import io.hbb.DAO.POJO.User.Profile;
import io.hbb.DAO.UserProfileDAO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;


import java.text.ParseException;

import static org.mockito.Mockito.when;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ PasswordEncoder.class, UserProfileDAO.class})
@ActiveProfiles("dev")
public class userProfileServiceTest {
    private static final Logger logger = LoggerFactory.getLogger(userProfileServiceTest.class);
    private UserProfileService userProfileService;
    private Profile oldProfile;

    @Before
    public void init() throws ParseException, UpdateFail, KeyDuplicated {
        String name =  "0961358196";
        String password = "password";
        String email = "wyatth@gmail.com";
        String familyName = "陳";
        String givenName ="愛睏";
        String address = "新北市";

        oldProfile = new Profile(name, password, email, familyName, givenName, address, "2000-1-1");
        PowerMockito.mockStatic(UserProfileDAO.class);
        when(UserProfileDAO.getUserProfile(1)).thenReturn(oldProfile);
        when(UserProfileDAO.updateUserProfile(1, oldProfile)).thenReturn(0);

        PasswordEncoder passwordEncoder = PowerMockito.mock(PasswordEncoder.class);
        PowerMockito.mockStatic(PasswordEncoder.class);
        when(passwordEncoder.encode(Mockito.anyString())).thenReturn("######");

        userProfileService = new UserProfileService();
        userProfileService.setPasswordEncoder(passwordEncoder);

    }

    @Test(expected = waitSMSValidate.class)
    public void testUpdateNameSuccess() throws Exception {

        Profile newProfile = new Profile(oldProfile);
        newProfile.setName("0952678627");

        userProfileService.updateUserProfile(1, newProfile);
    }


}
