package io.hbb.BO.config;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Calendar;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BusinessRule.class)
@ActiveProfiles("dev")
public class configTests {

    @Autowired
    private Environment env;

    @Autowired
    Calendar calendar;

    @Test
    public void environmentIsNotNull() throws Exception {
        Assert.assertNotNull(env);
    }

    @Test
    public void timezoneIsCorrect() throws Exception {
        Assert.assertEquals("Asia/Taiwan",env.getProperty("bo.timezone"));
    }

    @Test
    public void calenderIsNotNull() throws Exception {
        Assert.assertNotNull(calendar);
    }
}
