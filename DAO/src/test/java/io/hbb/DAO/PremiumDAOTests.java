package io.hbb.DAO;


import io.hbb.DAO.POJO.Premium.AvailableCreditCard;
import io.hbb.DAO.POJO.Premium.City;
import io.hbb.DAO.POJO.Premium.CreditCard;
import io.hbb.DAO.POJO.Premium.PremiumRaw;
import io.hbb.DAO.config.DatabaseDev;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.*;
import java.sql.Date;

import static io.hbb.DAO.PremiumDAO.*;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DatabaseDev.class)
@ActiveProfiles("dev")
public class PremiumDAOTests {

    private static Logger logger = LoggerFactory.getLogger(PremiumDAOTests.class);
    @Autowired
    @Qualifier("sql")
    private Properties properties;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private PremiumDAO premiumDAO;

    @Before
    public void init() {
        PremiumDAO premiumDAO = new PremiumDAO();
        premiumDAO.setJdbcTemplate(jdbcTemplate);
        premiumDAO.setSQL(properties);
    }

    @Test
    public void getCitySuccessTest() {
        List<City> cities = getCities();
        Set<String> s = new HashSet<String>();
        String[] c = {"台北市", "新北市", "新竹市", "台中市",
                "桃園市", "苗栗縣", "嘉義市", "台南市",
                "高雄市", "屏東縣", "宜蘭縣", "花蓮縣"};
        s.addAll(Arrays.asList(c));

        for (City cc : cities) {
            Assert.assertTrue(s.contains(cc.getName()));
        }
    }

    @Test
    public void getUserCreditCardsSuccessTest() {
        int userId = 1;
        int setId = 1;
        List<CreditCard> cards;
        cards = getUserCreditCards(userId, setId);
        Set<String> s = new HashSet<String>();
        String[] b = {"國泰世華銀行", "遠東國際商業銀行","永豐商業銀行"};
        String cardType = "未指定";
        s.addAll(Arrays.asList(b));
        for(CreditCard c: cards) {
            Assert.assertTrue(s.contains(c.getBank()));
            Assert.assertEquals(cardType, c.getCreditCard());
        }
    }

    @Test
    public void getUserCreditCardsFailTest() {
        int userId = 65535;
        int setId =1;
        List<CreditCard> cards;
        cards = getUserCreditCards(userId, setId);
        Assert.assertEquals(0, cards.size());
    }

    @Test
    public void getAvailableCreditCardSuccess() throws Exception{
        List<AvailableCreditCard> cards;
        cards = getAvailableCreditCards();
        Set<String> s = new HashSet<String>();
        String[] b = {"國泰世華銀行", "遠東國際商業銀行","花旗(台灣)商業銀行","玉山商業銀行","台北富邦銀行",
                "永豐商業銀行", "彰化商業銀行", "台灣銀行", "土地銀行", "合作金庫商業銀行", "第一商業銀行",
                "華南商業銀行", "上海商業儲蓄銀行", "高雄銀行", "兆豐國際商業銀行", "台灣中小企業銀行",
                "台中商業銀行", "匯豐(台灣)商業銀行", "華泰商業銀行", "台灣新光銀行", "陽信商業銀行",
                "三信商業銀行", "聯邦商業銀行", "元大銀行", "凱基銀行", "星展銀行(台灣)",
                "台新商業銀行", "日盛國際商業銀行", "安泰商業銀行", "中國信託商業銀行",
                "台灣樂天信用卡股份有限公司", "台灣永旺信用卡股份有限公司","中華郵政股份有限公司", "台灣美國運通國際(股)公司" };
        String cardType = "未指定";
        s.addAll(Arrays.asList(b));
        for(AvailableCreditCard c : cards) {
            Assert.assertTrue(s.contains(c.getBank().getName()));
            for(CreditCard cc: c.getCreditcards()) {
                //Assert.assertEquals(c.getBank().getName(), cc.getBank());
                logger.info("{}: {} {}",c.getBank().getName(), cc.getId(),cc.getCreditCard());
            }
        }
    }

    @Test
    public void getPremiumRawSuccessTest() throws Exception {
        int bankId = 1;
        Date d = Date.valueOf("2018-12-20");
        List<PremiumRaw> pr;
        pr = getPremiums(bankId, d, 1);
        for(PremiumRaw p : pr) {
            logger.info(p.getDepartment_name());
        }
    }

    @Test
    public void getPremiumRawNoSuchBankFailTest() throws Exception {
        int bankId = 65535;
        Date d = Date.valueOf("2018-12-20");
        List<PremiumRaw> pr;
        pr = getPremiums(bankId, d, 1);
        Assert.assertEquals(0, pr.size());
    }

    @Test
    public void getPremiumRawDateFailTest() throws Exception {
        int bankId = 1;
        Date d = Date.valueOf("2118-12-20");
        List<PremiumRaw> pr;
        pr = getPremiums(bankId, d, 1);
        Assert.assertEquals(0, pr.size());
    }

    @Test
    public void getPremiumRawCityFailTest() throws Exception {
        int bankId = 1;
        Date d = Date.valueOf("2018-12-20");
        List<PremiumRaw> pr;
        pr = getPremiums(bankId, d, 2);
        Assert.assertEquals(0, pr.size());
    }

    @Test
    public void setUserCreditCardsSuccessTest() throws Exception {
        int userId = 2;
        int setId = 1;
        int limit = 5;
        int[] cardsId = new int[] {1,2};
        List cards;
        setUserCreditCards(userId, setId, cardsId, limit);
        cards =  getUserCreditCards(userId, setId);
        Assert.assertEquals(2, cards.size());
    }

    @Test
    public void setUserCreditCardsNoSuchCardIdFailTest() throws Exception {
        //Partial update
        int userId = 2;
        int setId = 1;
        int limit = 5;
        int[] cardsId = new int[] {1,65535};
        List cards;
        setUserCreditCards(userId, setId, cardsId, limit);
        cards =  getUserCreditCards(userId, setId);
        Assert.assertEquals(1, cards.size());
    }
}
