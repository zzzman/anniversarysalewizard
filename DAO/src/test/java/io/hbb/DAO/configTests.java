package io.hbb.DAO;
import io.hbb.DAO.config.DatabaseDev;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.Properties;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DatabaseDev.class)
@ActiveProfiles("dev")
public class configTests {
    @Autowired
    @Qualifier("sql")
    private Properties properties;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    public void propertiesIsNotNull() throws Exception {
        Assert.assertNotNull(properties);
    }

    @Test
    public void jdbcTemplateIsNotNull() throws Exception {
        Assert.assertNotNull(jdbcTemplate);
    }

    @Test
    public void jdbcTemplateSomkeTest() throws Exception {
        jdbcTemplate.execute("select 1;");
    }


}
