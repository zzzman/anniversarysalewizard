package io.hbb.DAO;


import io.hbb.DAO.Exception.KeyDuplicated;
import io.hbb.DAO.Exception.UpdateFail;
import io.hbb.DAO.POJO.User.Profile;
import io.hbb.DAO.config.DatabaseDev;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.rowset.serial.SerialBlob;
import java.security.SecureRandom;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.Random;

import static io.hbb.DAO.UserProfileDAO.*;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DatabaseDev.class)
@ActiveProfiles("dev")
public class UserProfileDAOTests {

    @Autowired
    @Qualifier("sql")
    private Properties properties;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private UserProfileDAO userProfileDAO;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Before
    public void init() {
        userProfileDAO = new UserProfileDAO();
        userProfileDAO.setJdbcTemplate(jdbcTemplate);
        userProfileDAO.setSQL(properties);
    }

    @Test
    public void userProfileIdExists() throws Exception {
        Profile p = getUserProfile(1);
        Assert.assertEquals("0961358196",p.getName());
    }

    @Test
    public void userProfileIdNotExists() throws Exception {
        Profile p = getUserProfile(65536);
        Assert.assertNull(p.getName());
    }

    @Test
    public void userNameIsMeExitsExcludeMe() throws Exception {
        Assert.assertFalse(isUsernameExistedExcludeMe("0961358196", 1));
    }

    @Test
    public void userNameNotMeExitsExcludeMe() throws Exception {
        Assert.assertTrue(isUsernameExistedExcludeMe("0961358196", 2));
    }

    @Test
    public void userNameExists() throws Exception {
        Assert.assertTrue(isUsernameExisted("0961358196"));
    }

    @Test
    public void userNameNotExists() throws Exception {
        Assert.assertFalse(isUsernameExisted("0966111222"));
    }

    @Test
    public void emailExists() throws Exception {
        Assert.assertTrue(isEmailExisted("test@test.com"));
    }

    @Test
    public void familynameExists() throws Exception {
        Assert.assertTrue(isEmailExisted("test@test.com"));
    }

    @Test
    public void emailNotExists() throws Exception {
        Assert.assertFalse(isEmailExisted("nosuchuser@test.com"));
    }

    @Test
    public void getUserProfileViaNameSuccess() throws Exception {
        Profile p = getUserProfileViaName("0961358196");

        Assert.assertEquals("王", p.getFamilyName());
        Assert.assertEquals("測試", p.getGivenName());
        Assert.assertEquals(sdf.parse("1990-12-31"), p.getBirthday());
        Assert.assertEquals("新北市土城區裕民路11巷22號5樓",p.getAddress());
        Assert.assertEquals("0961358196",p.getName());
        Assert.assertEquals("test@test.com",p.getEmail());
        Assert.assertEquals("$2a$10$0zD2GwX5cALCjMdxfxf9f.2r/klWCrb3tZ39D/WwR5AEGmkKPbDA6", p.getPassword());
    }

    @Test
    public void getUserProfileViaNameFail() throws Exception {
        Profile p = getUserProfileViaName("0911000000");
        Assert.assertNull(p.getName());
        Assert.assertNull(p.getEmail());
        Assert.assertNull(p.getPassword());
        Assert.assertNull(p.getBirthday());
        Assert.assertNull(p.getAddress());
        Assert.assertNull(p.getFamilyName());
        Assert.assertNull(p.getGivenName());
    }

    @Test
    public void userSignupSuccess() throws Exception {
        String name = "0911654321";
        String email = "newuser@test.com";
        String password = "$2a$10$0zD2GwX5cALCjMdxfxf9f.2r/klWCrb3tZ39D/WwR5AEGmkKPbDA6";
        String familyName = "新";
        String givenName = "測試";
        String address = "新北市測試路15號6樓";
        String birthday = "1990-6-1";
        Profile p = new Profile();
        Random RANDOM = new SecureRandom();
        byte[] salt = new byte[16];
        RANDOM.nextBytes(salt);
        p.setName(name);
        p.setEmail(email);
        p.setPassword(password);
        p.setSalt(new SerialBlob(salt));
        p.setFamilyName(familyName);
        p.setGivenName(givenName);
        p.setBirthday(new Date(sdf.parse(birthday).getTime()));
        p.setAddress(address);
        userSignup(p);

        p = getUserProfileViaName(name);
        Assert.assertEquals(name, p.getName());
        Assert.assertEquals(email,p.getEmail());
        Assert.assertEquals(password, p.getPassword());
        Assert.assertEquals(givenName, p.getGivenName());
        Assert.assertEquals(familyName, p.getFamilyName());
        Assert.assertEquals(address, p.getAddress());
        Assert.assertEquals(new Date(sdf.parse(birthday).getTime()), p.getBirthday());
    }

    @Test(expected= KeyDuplicated.class)
    public void userSignupFailDuplicateName() throws Exception {
        String name = "0961358196";
        String email = "newuser@test.com";
        String password = "$2a$10$0zD2GwX5cALCjMdxfxf9f.2r/klWCrb3tZ39D/WwR5AEGmkKPbDA6";
        String familyName = "新";
        String givenName = "測試";
        String address = "新北市測試路15號6樓";
        String birthday = "1990-6-1";
        Profile p = new Profile();
        Random RANDOM = new SecureRandom();
        byte[] salt = new byte[16];
        RANDOM.nextBytes(salt);
        p.setName(name);
        p.setEmail(email);
        p.setPassword(password);
        p.setSalt(new SerialBlob(salt));
        p.setFamilyName(familyName);
        p.setGivenName(givenName);
        p.setBirthday(new Date(sdf.parse(birthday).getTime()));
        p.setAddress(address);
        userSignup(p);
    }

    @Test(expected= KeyDuplicated.class)
    public void userSignupFailDuplicateEmail() throws Exception {
        String name = "0911654321";
        String email = "test@test.com";
        String password = "$2a$10$0zD2GwX5cALCjMdxfxf9f.2r/klWCrb3tZ39D/WwR5AEGmkKPbDA6";
        String familyName = "新";
        String givenName = "測試";
        String address = "新北市測試路15號6樓";
        String birthday = "1990-6-1";
        Profile p = new Profile();
        Random RANDOM = new SecureRandom();
        byte[] salt = new byte[16];
        RANDOM.nextBytes(salt);
        p.setName(name);
        p.setEmail(email);
        p.setPassword(password);
        p.setSalt(new SerialBlob(salt));
        p.setFamilyName(familyName);
        p.setGivenName(givenName);
        p.setBirthday(new Date(sdf.parse(birthday).getTime()));
        p.setAddress(address);
        userSignup(p);
    }

    @Test
    public void updateLoginTimeSuccess() throws Exception {
        int userid = 2;
        Profile oldP = getUserProfile(userid);
        updateLoginTime(userid);
        Profile newP = getUserProfile(userid);
        Assert.assertNotNull(newP.getLogin_time());
        Assert.assertNotEquals(oldP.getLogin_time(), newP.getLogin_time());
    }

    @Test(expected = UpdateFail.class)
    public void updateLoginTimeFail() throws Exception {
        int userid = 65535;
        updateLoginTime(userid);
    }

    @Test
    public void userProfileUpdateEmailSuccess() throws Exception{
        int userid = 2;
        String newEmail = "newemail@test.com";
        Profile oldP = getUserProfile(userid);
        Profile newP =new Profile(oldP);
        newP.setEmail(newEmail);
        updateUserProfile(userid, newP);
        Profile returnP = getUserProfile(userid);

        Assert.assertEquals(newP.getEmail(), newEmail);
        Assert.assertEquals(newP.getEmail(),returnP.getEmail());
        Assert.assertEquals(newP.getPassword(), oldP.getPassword());
        Assert.assertEquals(newP.getGivenName(), oldP.getGivenName());
        Assert.assertEquals(newP.getFamilyName(), oldP.getFamilyName());
        Assert.assertEquals(newP.getAddress(), oldP.getAddress());
        Assert.assertEquals(newP.getName(), oldP.getName());
        Assert.assertEquals(newP.getBirthday(), oldP.getBirthday());
    }

    @Test
    public void userProfileUpdatePasswordSuccess() throws Exception{
        int userid = 2;
        String newPassword = "123";
        Profile oldP = getUserProfile(userid);
        Profile newP =new Profile(oldP);
        newP.setPassword(newPassword);
        updateUserProfile(userid, newP);
        Profile returnP = getUserProfile(userid);

        Assert.assertEquals(newP.getEmail(),returnP.getEmail());
        Assert.assertEquals(newP.getPassword(), returnP.getPassword());
        Assert.assertEquals(newP.getGivenName(), returnP.getGivenName());
        Assert.assertEquals(newP.getFamilyName(), returnP.getFamilyName());
        Assert.assertEquals(newP.getAddress(), returnP.getAddress());
        Assert.assertEquals(newP.getName(), returnP.getName());
        Assert.assertEquals(newP.getBirthday(), oldP.getBirthday());
        Assert.assertEquals(newP.getEmail(),returnP.getEmail());
        Assert.assertNotEquals(returnP.getPassword(), oldP.getPassword());
        Assert.assertEquals(newP.getGivenName(), returnP.getGivenName());
    }

    @Test
    public void userProfileUpdateNameSuccess() throws Exception{
        int userid = 2;
        String newName = "0911999888";
        Profile oldP = getUserProfile(userid);
        Profile newP =new Profile(oldP);
        newP.setName(newName);
        updateUserProfile(userid, newP);
        Profile returnP = getUserProfile(userid);

        Assert.assertEquals(newP.getEmail(),returnP.getEmail());
        Assert.assertEquals(newP.getPassword(), returnP.getPassword());
        Assert.assertEquals(newP.getGivenName(), returnP.getGivenName());
        Assert.assertEquals(newP.getFamilyName(), returnP.getFamilyName());
        Assert.assertEquals(newP.getAddress(), returnP.getAddress());
        Assert.assertEquals(newName, returnP.getName());
        Assert.assertEquals(newP.getBirthday(), oldP.getBirthday());
        Assert.assertEquals(newP.getEmail(),returnP.getEmail());
        Assert.assertEquals(newP.getPassword(), returnP.getPassword());
        Assert.assertEquals(newP.getGivenName(), returnP.getGivenName());
    }

    @Test(expected = UpdateFail.class)
    public void userProfileUpdateFail() throws Exception{
        int userid = 65535;
        String newEmail = "test@test.com";
        Profile oldP = getUserProfile(userid);
        Profile newP =new Profile();
        newP.setPassword(oldP.getPassword());
        newP.setEmail(newEmail);
        updateUserProfile(userid, newP);
    }

    @Test(expected = KeyDuplicated.class)
    public void userProfileUpdateEmailFail() throws Exception{
        int userid = 2;
        String newEmail = "test@test.com";
        Profile oldP = getUserProfile(userid);
        Profile newP =new Profile(oldP);
        newP.setEmail(newEmail);
        updateUserProfile(userid, newP);
    }

    @Test(expected = KeyDuplicated.class)
    public void userProfileUpdateNamelFail() throws Exception{
        int userid = 2;
        String newName = "0961358196";
        Profile oldP = getUserProfile(userid);
        Profile newP =new Profile(oldP);
        newP.setName(newName);
        updateUserProfile(userid, newP);
    }
}
