package io.hbb.DAO.Util;

import io.hbb.DAO.POJO.Advertisement.Advertisement;
import io.hbb.DAO.POJO.Advertisement.Department;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class AdvertisementFactory {

    private static Logger logger = LoggerFactory.getLogger(AdvertisementFactory.class);

    public static Department getDepartment(ResultSet rs) throws SQLException {
        Department d = new Department();
        d.setArea(rs.getString("area"));
        d.setGroup(rs.getString("group_name"));
        d.setDepartment(rs.getString("department"));
        d.setUrl(rs.getString("url"));
        return d;
    }

    public static Advertisement getAdvertisement(ResultSet rs) throws SQLException {
        Advertisement a = new Advertisement();
        a.setZone(rs.getInt("zone"));
        a.setSort(rs.getInt("sort"));
        a.setImg(rs.getString("img"));
        a.setText(rs.getString("note"));
        a.setUrl(rs.getString("url"));
        return a;
    }
}
