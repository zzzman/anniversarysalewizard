package io.hbb.DAO.Util;

import io.hbb.DAO.POJO.Premium.AvailableCreditCard;
import io.hbb.DAO.POJO.Premium.CreditCard;
import io.hbb.DAO.POJO.User.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProfileFactory {
    private static final Logger logger = LoggerFactory.getLogger(ProfileFactory.class);

    public static Profile getProfile(ResultSet rs) throws SQLException {
        Profile p = new Profile();
        p.setId(rs.getInt("id"));
        p.setName(rs.getString("name"));
        p.setFamilyName(rs.getString("family_name"));
        p.setGivenName(rs.getString("given_name"));
        p.setBirthday(rs.getDate("birthday"));
        p.setAddress(rs.getString("address"));
        p.setEmail(rs.getString("email"));
        p.setIs_activated(rs.getBoolean("is_activated"));
        p.setLogin_time(rs.getTimestamp("login_time"));
        p.setModify_time(rs.getTimestamp("modify_time"));
        p.setPassword(rs.getString("password"));
        p.setRegister_time(rs.getTimestamp("register_time"));
        p.setSalt(rs.getBlob("salt"));
        p.setActivate_time(rs.getTimestamp("activate_time"));
        return p;
    }


}
