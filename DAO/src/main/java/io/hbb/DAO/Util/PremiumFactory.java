package io.hbb.DAO.Util;

import io.hbb.DAO.POJO.Premium.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class PremiumFactory {
    private static final Logger logger = LoggerFactory.getLogger(PremiumFactory.class);

    public static City getCity(ResultSet rs) throws SQLException {
        City c = new City();
        c.setId(rs.getInt("id"));
        c.setName(rs.getString("name"));
        return c;
    }

    public static CreditCard getUserCreditCard(ResultSet rs) throws SQLException {
        CreditCard c = new CreditCard();
        c.setId(rs.getInt("id"));
        c.setBank(rs.getString("bank"));
        c.setCreditCard(rs.getString("credit_card"));
        return c;
    }

    public static PremiumRaw getPremium(ResultSet rs) throws SQLException {
        PremiumRaw p = new PremiumRaw();
        p.setId(rs.getInt("id"));
        p.setBonus(rs.getInt("bonus"));
        p.setBounding(rs.getInt("bounding"));
        p.setPrice(rs.getInt("price"));
        p.setBank_name(rs.getString("bank_name"));
        p.setBegin_time(rs.getDate("begin_time"));
        p.setEnd_time(rs.getDate("end_time"));
        p.setCampaign_name(rs.getString("campaign_name"));
        p.setDescription(rs.getString("description"));
        p.setCredit_card_name(rs.getString("credit_card_name"));
        p.setUrl(rs.getString("url"));
        //String departmentGroupName =  rs.getString("department_group_name");
        p.setDepartment_name(rs.getString("department_name"));
        return p;
    }

    public static AvailableCreditCard getAvailableCreditCard(ResultSet rs) throws SQLException {
        AvailableCreditCard p = new AvailableCreditCard();
        Bank b = new Bank();
        List<CreditCard> cs = new LinkedList<>();

        b.setId(rs.getInt("bank_id"));
        b.setName(rs.getString("bank_name"));
        p.setBank(b);

        String cards = rs.getString("credit_cards");

        if (cards == null) {
            logger.error("bank id: {} ({}) has null credit card", b.getId(), b.getName());
            return null;
        }

        if (cards.contains("|")) {
            String[] tmp = cards.split("\\|");
            for (String t : tmp) {
                CreditCard card = new CreditCard();
                String[] tt = t.split(",");
                card.setId(Integer.valueOf(tt[0]));
                card.setCreditCard(tt[1]);
                cs.add(card);
            }
        } else {
            //only single card
            CreditCard card = new CreditCard();
            String[] tt = cards.split(",");
            card.setId(Integer.valueOf(tt[0]));
            card.setCreditCard(tt[1]);
            cs.add(card);
        }

        p.setCreditcards(cs);
        return p;
    }
}
