package io.hbb.DAO;

import io.hbb.DAO.Exception.*;
import io.hbb.DAO.POJO.User.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import java.sql.*;
import java.util.List;
import java.util.Properties;

import static io.hbb.DAO.Util.ProfileFactory.getProfile;

@Repository
public class UserProfileDAO {
    private static JdbcTemplate jdbcTemplate;
    private static Properties properties;
    private static Logger logger = LoggerFactory.getLogger(UserProfileDAO.class);

    @Autowired
    public void setJdbcTemplate(JdbcTemplate j) {
        jdbcTemplate = j;
    }

    @Autowired
    @Qualifier("sql")
    public void setSQL(Properties p) {
        properties = p;
    }

    public static Profile getUserProfile(int userId) {

        try{
            return jdbcTemplate.queryForObject(
                    properties.getProperty("get_user_profile"),
                    new RowMapper<Profile>() {
                        @Override
                        public Profile mapRow(ResultSet resultSet, int i) throws SQLException {
                            return getProfile(resultSet);
                        }
                    },
                    userId
            );
        } catch (EmptyResultDataAccessException e) {
            return new Profile();
        }
    }

    public static boolean isUsernameExistedExcludeMe(String username, int userId) {
        try {
            return jdbcTemplate.queryForObject(
                    properties.getProperty("is_username_existed_exclude_me"),
                    new RowMapper<Boolean>() {
                        @Override
                        public Boolean mapRow(ResultSet resultSet, int i) throws SQLException {
                            int r = resultSet.getInt(1);
                            return r == 1 ? true : false;
                        }
                    },
                    username,
                    userId
            );
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    public static boolean isEmailExistedExcludeMe(String email, int userId) {

        try{
            return jdbcTemplate.queryForObject(
                    properties.getProperty("is_email_existed_exclude_me"),
                    new RowMapper<Boolean>() {
                        @Override
                        public Boolean mapRow(ResultSet resultSet, int i) throws SQLException {
                            int r = resultSet.getInt(1);
                            return r == 1 ? true : false;
                        }
                    },
                    email,
                    userId
            );
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
    public static boolean isUsernameExisted(String username) {
        try {
            return jdbcTemplate.queryForObject(
                    properties.getProperty("is_username_existed"),
                    new RowMapper<Boolean>() {
                        @Override
                        public Boolean mapRow(ResultSet resultSet, int i) throws SQLException {
                            int r = resultSet.getInt(1);
                            return r == 1 ? true : false;
                        }
                    },
                    username
            );
        } catch (EmptyResultDataAccessException e) {
           return false;
        }
    }

    public static boolean isEmailExisted(String email) {

        try{
            return jdbcTemplate.queryForObject(
                    properties.getProperty("is_email_existed"),
                    new RowMapper<Boolean>() {
                        @Override
                        public Boolean mapRow(ResultSet resultSet, int i) throws SQLException {
                            int r = resultSet.getInt(1);
                            return r == 1 ? true : false;
                        }
                    },
                    email
            );
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    public static void userSignup(Profile user) throws KeyDuplicated {

        try {
            int r = jdbcTemplate.update(properties.getProperty("user_sign_up"),
                    user.getName(),
                    user.getFamilyName(),
                    user.getGivenName(),
                    user.getAddress(),
                    user.getBirthday(),
                    user.getPassword(),
                    user.getSalt(),
                    user.getEmail(),
                    false //waiting for active
            );
            if (r <= 0) {
                logger.error("Sign up fail data: {}",
                        user.toString()
                );
            }
        } catch (DuplicateKeyException e) {
            throw new KeyDuplicated(e.getMessage());
        }
    }

    public static Profile getUserProfileViaName(String name) {

        try{
            return jdbcTemplate.queryForObject(
                    properties.getProperty("get_user_profile_via_name"),
                    new RowMapper<Profile>() {
                        @Override
                        public Profile mapRow(ResultSet resultSet, int i) throws SQLException {
                            return getProfile(resultSet);
                        }
                    },
                    name
            );
        } catch (EmptyResultDataAccessException e) {
            return new Profile();
        }
    }

    public static void updateLoginTime(int userId) throws UpdateFail {

        int r = jdbcTemplate.update(properties.getProperty("update_user_login_time"),
                userId
        );
        if(r <= 0) {
            logger.error("Update login time fail, user id:{}", userId );
            throw new UpdateFail("login time update fail");
        }
    }

    public static void turnActivate(int userId, boolean flag) throws UpdateFail {
        String cmd = flag == true? "turn_on_user_activated": "turn_off_user_activated";

        int r = jdbcTemplate.update(properties.getProperty(cmd),
                flag
        );
        if (r <= 0) {
            logger.error("cmd {} fail, id: {}, flag: {}", cmd, userId, flag);
            throw new UpdateFail("Profile update fail");
        }
    }

    public static int updateUserProfile(int userId, Profile profile) throws UpdateFail, KeyDuplicated {

        try {
            int r = jdbcTemplate.update(properties.getProperty("update_user_profile"),
                    profile.getName(),
                    profile.getEmail(),
                    profile.getFamilyName(),
                    profile.getGivenName(),
                    profile.getAddress(),
                    profile.getPassword(),
                    profile.getIs_activated(),
                    userId
            );
            if (r <= 0) {
                logger.error("Update user profile fail, input data: {} id: {}", profile.toString(), userId);
                throw new UpdateFail("Profile update fail");
            }
        } catch (DuplicateKeyException e) {
            throw new KeyDuplicated("email: "+ profile.getEmail() + " or Name:" + profile.getName() + " existed ");
        }

        return 0;
    }

    public static void setUserSMSCode(int userId, String sms, String ip) throws SendTooManySMS, UpdateFail, UserHasActivated {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(jdbcTemplate.getDataSource());
        TransactionTemplate transactionTemplate =  new TransactionTemplate(transactionManager);
        int retCode =  transactionTemplate.execute(
                (TransactionCallback<Integer>) transactionStatus -> {
                    Profile p;
                    try{
                        p = jdbcTemplate.queryForObject(
                                properties.getProperty("get_user_profile"),
                                new RowMapper<Profile>() {
                                    @Override
                                    public Profile mapRow(ResultSet resultSet, int i) throws SQLException {
                                        return getProfile(resultSet);
                                    }
                                },
                                userId
                        );
                    } catch (EmptyResultDataAccessException e) {
                        return -2;
                    }

                    if(p.getIs_activated() == true) {
                        return -3;
                    }


                    List<Integer> sandSMS =
                        jdbcTemplate.queryForList(
                                properties.getProperty("check_user_can_send_sms"),
                                new Object[]{userId},
                               Integer.TYPE );


                    if(sandSMS.size()!= 0 && sandSMS.get(0) > 2) {
                        return -1;
                    }

                    int r = jdbcTemplate.update(properties.getProperty("set_user_sms_code"),
                            sms,
                            ip,
                            userId
                    );
                    if (r <= 0) {
                        logger.error("Set user sms fail, input data: {} id: {}", sms, userId);
                        return -2;
                    }
                    return r;
                }
        );
        if(retCode == -1) {
            throw new SendTooManySMS();
        }
        if(retCode == -2) {
            throw new UpdateFail("Set user sms fail");
        }
        if(retCode == -3) {
            throw new UserHasActivated();
        }
        return;
    }

    public static void validateSMS(int userId, String sms) throws UpdateFail, invalidSMS {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(jdbcTemplate.getDataSource());
        TransactionTemplate transactionTemplate =  new TransactionTemplate(transactionManager);
        int retCode =  transactionTemplate.execute(
                (TransactionCallback<Integer>) transactionStatus -> {
                    List<Integer> passedSMS =
                            jdbcTemplate.queryForList(
                                    properties.getProperty("validate_user_latest_sms"),
                                    new Object[]{userId, sms},
                                    Integer.TYPE);

                    if (passedSMS.size() != 0 && passedSMS.get(0) != 1) {
                        return -1;
                    }

                    int r = jdbcTemplate.update(properties.getProperty("turn_on_user_activated"),
                            userId
                    );
                    if (r <= 0) {
                        logger.error("Turn on activate status fail in validate sms, id: {}", userId);
                        return -2;
                    }
                    return 0;
                }
        );

        if(retCode == -1) {
            throw new invalidSMS();
        }
        if(retCode == -2) {
            throw new UpdateFail("validate and update user sms fail");
        }
        return;

    }


    public static void setChangePasswordHash(int userId, String email, String hash) {
        int r = jdbcTemplate.update(properties.getProperty("set_change_password_mail_code"),
                userId, email, hash
        );
        if (r <= 0) {
            logger.error("Save change password hash code fail, userId: {}, email: {}, hash: {}",
                    userId, email, hash
            );
        }
    }

    public static void updatePasswordViaHash(String hash, String password) {
        int r = jdbcTemplate.update(properties.getProperty("update_password_via_hash"),
                password, hash
        );
        if (r < 0) {
            logger.error("Update password via hash code fail, hash: {}", hash);
        }
    }

}
