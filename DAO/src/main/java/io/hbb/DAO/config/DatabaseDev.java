package io.hbb.DAO.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@PropertySource("classpath:database-dev.properties")
@Profile("dev")
public abstract class DatabaseDev extends Database {
    private static Logger log = LoggerFactory.getLogger(DatabaseDev.class);

    @Autowired
    private Environment env;

    @Bean
    //@Scope("prototype")
    public DataSource getDatasource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .setScriptEncoding("UTF-8")
                .setName(env.getProperty("spring.datasource.url"))
                .addScript("classpath:sql/schemaDev.sql")
                .build();
    }

    @Bean(name ="sql")
    public Properties getSQL() throws Exception {
        return super.getSQL();
    }

    @Bean
    public JdbcTemplate getJdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

}
