package io.hbb.DAO.config;

import javax.sql.DataSource;
import java.io.InputStream;
import java.util.Properties;

public abstract class Database {
    protected Properties readProperties(String xmlFileName) throws Exception {
        Properties p = new Properties();
        InputStream is = DatabaseProd.class.getResourceAsStream(xmlFileName);
        p.loadFromXML(is);
        return p;
    }

    protected Properties getSQL() throws Exception {
        Properties p = new Properties();
        try {
            p = readProperties("/sql.xml");
        } catch (Exception e) {
            throw e;
        }
        return p;
    }

    public abstract DataSource getDatasource();

}
