package io.hbb.DAO.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Properties;

@Configuration
@PropertySource("classpath:database-prod.properties")
public class DatabaseProd extends Database{

    private static Logger log = LoggerFactory.getLogger(DatabaseProd.class);
    @Autowired
    private Environment env;

    @Bean
    public DataSource getDatasource() {
        DataSource dataSource = new DataSource();
        PoolProperties p = new PoolProperties();
        p.setJmxEnabled(true);
        p.setTestWhileIdle(false);
        p.setTestOnBorrow(true);
        p.setValidationQuery("SELECT 1");
        p.setTestOnReturn(false);
        p.setValidationInterval(30000);
        p.setTimeBetweenEvictionRunsMillis(30000);
        p.setMaxActive(100);
        p.setInitialSize(10);
        p.setMaxWait(10000);
        p.setRemoveAbandonedTimeout(60);
        p.setMinEvictableIdleTimeMillis(30000);
        p.setMinIdle(10);
        p.setLogAbandoned(true);
        p.setRemoveAbandoned(true);
        p.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"+"org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
        p.setDriverClassName(env.getProperty("spring.datasource.driver_class_name"));
        p.setUrl(env.getProperty("spring.datasource.url"));
        p.setPassword(env.getProperty("spring.datasource.password"));
        p.setUsername(env.getProperty("spring.datasource.username"));
        dataSource.setDefaultAutoCommit(false);
        dataSource.setPoolProperties(p);
        return dataSource;
    }

    @Bean(name ="sql")
    public Properties getSQL() throws Exception {
        return super.getSQL();
    }

    @Bean
    public JdbcTemplate getJdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

}
