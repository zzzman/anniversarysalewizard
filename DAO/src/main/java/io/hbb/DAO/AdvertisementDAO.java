package io.hbb.DAO;

import io.hbb.DAO.POJO.Advertisement.Advertisement;
import io.hbb.DAO.POJO.Advertisement.Department;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import static io.hbb.DAO.Util.AdvertisementFactory.getAdvertisement;
import static io.hbb.DAO.Util.AdvertisementFactory.getDepartment;

@Repository
public class AdvertisementDAO {

    private static JdbcTemplate jdbcTemplate;
    private static Properties properties;
    private static Logger logger = LoggerFactory.getLogger(AdvertisementDAO.class);

    @Autowired
    public void setJdbcTemplate(JdbcTemplate j) {
        jdbcTemplate = j;
    }

    @Autowired
    @Qualifier("sql")
    public void setSQL(Properties p) {
        properties = p;
    }

    static public List<Advertisement> getAdv() {
        try{
            return jdbcTemplate.query(
                    properties.getProperty("get_advertisement"),
                    new RowMapper<Advertisement>() {
                @Override
                public Advertisement mapRow(ResultSet resultSet, int i) throws SQLException {
                    return getAdvertisement(resultSet);
                }
            });
        } catch (EmptyResultDataAccessException e) {
            return new LinkedList<>();
        }
    }

    static public List<Department> getAllDepartment() {
        try{
            return jdbcTemplate.query(
                    properties.getProperty("get_department_list"),
                    new RowMapper<Department>() {
                        @Override
                        public Department mapRow(ResultSet resultSet, int i) throws SQLException {
                            return getDepartment(resultSet);
                        }
                    }
                    );
        } catch (EmptyResultDataAccessException e) {
            return new LinkedList<>();
        }
    }

}
