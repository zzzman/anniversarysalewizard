package io.hbb.DAO;

import io.hbb.DAO.Exception.TooManyCard;
import io.hbb.DAO.POJO.Premium.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import static io.hbb.DAO.Util.PremiumFactory.*;

@Repository
public class PremiumDAO {

    private static JdbcTemplate jdbcTemplate;
    private static Properties properties;
    private static Logger logger = LoggerFactory.getLogger(PremiumDAO.class);

    @Autowired
    public void setJdbcTemplate(JdbcTemplate j) {
        jdbcTemplate = j;
    }

    @Autowired
    @Qualifier("sql")
    public void setSQL(Properties p) {
        properties = p;
    }

    static public List<City> getCities() {
        try{
            return jdbcTemplate.query(
                    properties.getProperty("get_city"),
                    new RowMapper<City>() {
                @Override
                public City mapRow(ResultSet resultSet, int i) throws SQLException {
                    return getCity(resultSet);
                }
            });
        } catch (EmptyResultDataAccessException e) {
            return new LinkedList<>();
        }
    }

    static public List<CreditCard> getUserCreditCards(int userId, int setId) {
        try{
            return jdbcTemplate.query(
                    properties.getProperty("get_user_credit_card"),
                    new RowMapper<CreditCard>() {
                        @Override
                        public CreditCard mapRow(ResultSet resultSet, int i) throws SQLException {
                            return getUserCreditCard(resultSet);
                        }
                    },
                    userId,
                    setId
                    );
        } catch (EmptyResultDataAccessException e) {
            return new LinkedList<>();
        }
    }

    static public void setUserCreditCards(int userId, int setId, int[] cards, int limit) throws TooManyCard {
        if(cards.length > limit) {
            throw new TooManyCard("user id(" + userId + ")'s credit cards > " + limit);
        }
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(jdbcTemplate.getDataSource());
        TransactionTemplate transactionTemplate =  new TransactionTemplate(transactionManager);
        transactionTemplate.execute(
                new TransactionCallback<Object>() {
                    @Override
                    public Object doInTransaction(TransactionStatus transactionStatus) {
                        jdbcTemplate.update(properties.getProperty("delete_user_credit_card"),
                                userId, setId);
                        BatchPreparedStatementSetter setter =
                                new BatchPreparedStatementSetter() {
                                    public void setValues(PreparedStatement stmt, int i) throws SQLException {
                                        stmt.setInt(1,userId);
                                        stmt.setInt(2,setId);
                                        stmt.setInt(3,cards[i]);
                                    }
                                    public int getBatchSize() {
                                        return cards.length;
                                    }
                                };
                        jdbcTemplate.batchUpdate(properties.getProperty("set_user_credit_card"), setter);
                        return null;
                    }
                }
        );
        /*
        PreparedStatement stmt = null;
        Connection c = null;
        try  {
            c = jdbcTemplate.getDataSource().getConnection();
            c.setAutoCommit(false);

            stmt = c.prepareStatement(properties.getProperty("delete_user_credit_card"));
            stmt.setInt(1, userId);
            stmt.setInt(2, setId);
            stmt.execute();

            stmt = c.prepareStatement(properties.getProperty("set_user_credit_card"));
            stmt.clearBatch();
            for(int cid : cards) {
                stmt.setInt(1,userId);
                stmt.setInt(2,setId);
                stmt.setInt(3,cid);
                stmt.addBatch();
               // logger.info(" {} {} {} ", userId,setId,cid);
            }
            stmt.executeBatch();
            c.commit();
        } catch (SQLException e) {
            try { c.rollback(); }
            catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            if(stmt != null) {
                try { stmt.close(); }
                catch(SQLException e) {
                    e.printStackTrace();
                }
            }
            if(c != null) {
                try { c.close(); }
                catch(SQLException e) {
                    e.printStackTrace();
                }
            }
        }
*/
    }

    static public List<AvailableCreditCard> getAvailableCreditCards() {
        try{
            return jdbcTemplate.query(
                    properties.getProperty("get_available_credit_card"),
                    new RowMapper<AvailableCreditCard>() {
                        @Override
                        public AvailableCreditCard mapRow(ResultSet resultSet, int i) throws SQLException {
                            return getAvailableCreditCard(resultSet);
                        }
                    });
        } catch (EmptyResultDataAccessException e) {
            return new LinkedList<>();
        }
    }


    static public List<PremiumRaw> getPremiums(int userId, Date date, int city) {
        try{
            return jdbcTemplate.query(
                    properties.getProperty("query_premium_via_bank_id"),
                    new RowMapper<PremiumRaw>() {
                        @Override
                        public PremiumRaw mapRow(ResultSet resultSet, int i) throws SQLException {
                            return getPremium(resultSet);
                        }
                    },
                    userId, date, city
            );
        } catch (EmptyResultDataAccessException e) {
            return new LinkedList<>();
        }
    }

}
