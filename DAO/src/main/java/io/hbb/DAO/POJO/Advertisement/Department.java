package io.hbb.DAO.POJO.Advertisement;


public class Department {
    private String area;
    private String group;
    private String department;
    private String url;

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        String format = "area: %s, group: %s, department: %s, url: %s";
        return String.format(format, area, group, department, url);
    }
}
