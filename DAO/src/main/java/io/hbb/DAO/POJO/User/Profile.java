package io.hbb.DAO.POJO.User;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Blob;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Profile {

    private String name;
    private String email;
    private String familyName;
    private String givenName;
    private String address;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "UTC+8")
    private Date birthday;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC+8")
    private Timestamp login_time;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC+8")
    private Timestamp register_time;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC+8")
    private Timestamp modify_time;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC+8")
    private Timestamp activate_time;

    @JsonIgnore
    private int id;
    @JsonIgnore
    private boolean is_activated;

    private String password;
    @JsonIgnore
    private Blob salt;

    public Profile(){}

    public Profile(Profile p) {
        setAddress(p.getAddress());
        setBirthday(p.getBirthday());
        setGivenName(p.getGivenName());
        setFamilyName(p.getFamilyName());
        setEmail(p.getEmail());
        setPassword(p.getPassword());
        setName(p.getName());
        setSalt(p.getSalt());
    }

    public Profile(String name, String password, String email,
                   String familyName, String givenName, String address,
                   String birthday) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        setName(name);
        setPassword(password);
        setEmail(email);
        setFamilyName(familyName);
        setGivenName(givenName);
        setAddress(address);
        setBirthday(new Date(sdf.parse(birthday).getTime()));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @JsonProperty
    public Timestamp getLogin_time() {
        return login_time;
    }

    @JsonIgnore
    public void setLogin_time(Timestamp login_time) {
        this.login_time = login_time;
    }

    @JsonProperty
    public Timestamp getRegister_time() {
        return register_time;
    }

    @JsonIgnore
    public void setRegister_time(Timestamp register_time) {
        this.register_time = register_time;
    }

    @JsonProperty
    public Timestamp getModify_time() {
        return modify_time;
    }

    @JsonIgnore
    public void setModify_time(Timestamp modify_time) {
        this.modify_time = modify_time;
    }

    public boolean getIs_activated() {
        return is_activated;
    }

    public void setIs_activated(boolean is_activated) {
        this.is_activated = is_activated;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    public Blob getSalt() {
        return salt;
    }

    public void setSalt(Blob salt) {
        this.salt = salt;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @JsonProperty
    public Timestamp getActivate_time() {
        return activate_time;
    }

    @JsonIgnore
    public void setActivate_time(Timestamp activate_time) {
        this.activate_time = activate_time;
    }

    @Override
    public String toString() {
        return String.format(
                "name:%s, password:%s, email:%s, family_name:%s, given_name:%s, birthday:%s, address:%s",
                name, password, email, familyName, givenName, birthday, address
        );
    }
}
