package io.hbb.DAO.POJO.Premium;


import java.util.List;


public class AvailableCreditCard {
    private Bank bank;
    private List<CreditCard> creditcards;

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public List<CreditCard> getCreditcards() {
        return creditcards;
    }

    public void setCreditcards(List<CreditCard> creditcards) {
        this.creditcards = creditcards;
    }
}
