package io.hbb.DAO.POJO.Premium;

import java.sql.Date;

public class PremiumRaw {
    private int id;
    private int price;
    private int bonus;
    private int bounding;
    private Date begin_time;
    private Date end_time;
    private String campaign_name;
    private String description;
    private String credit_card_name;
    private String url;
    private String bank_name;
    private String department_name;
    final private static int price_rate = 100;
    final private static int bonus_rate = 10;



    public void PremiumRaw(PremiumRaw pr) {
        setUrl(pr.getUrl());
        setBank_name(pr.getBank_name());
        setBegin_time(pr.getBegin_time());
        setBonus(pr.getBonus()*bonus_rate);
        setBounding(pr.getBounding());
        setCampaign_name(pr.getCampaign_name());
        setCredit_card_name(pr.getCredit_card_name());
        setDescription(pr.getDescription());
        setEnd_time(pr.getEnd_time());
        setId(pr.getId());
        setPrice(pr.getPrice()*price_rate);
        setDepartment_name(pr.getDepartment_name());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    public int getBounding() {
        return bounding;
    }

    public void setBounding(int bounding) {
        this.bounding = bounding;
    }

    public Date getBegin_time() {
        return begin_time;
    }

    public void setBegin_time(Date begin_time) {
        this.begin_time = begin_time;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }

    public String getCampaign_name() {
        return campaign_name;
    }

    public void setCampaign_name(String campaign_name) {
        this.campaign_name = campaign_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCredit_card_name() {
        return credit_card_name;
    }

    public void setCredit_card_name(String credit_card_name) {
        this.credit_card_name = credit_card_name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }

    @Override
    public String toString() {
        String format ="id: %d, price: %d, bonus: %d, bounding: %d, " +
                "begin time: %s, end time: %s, campaign_name: %s, description: %s, credit_card_name: %s," +
                "url: %s, bank_name: %s, department_name: %s";
        return String.format(format,
        id, price, bonus, bounding,
                begin_time.toString(),
                end_time.toString(),
                campaign_name, description, credit_card_name,
                url, bank_name, department_name
                );
    }
}
