package io.hbb.DAO.POJO.Advertisement;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Advertisement {
    private int zone;
    private int sort;
    private String img;
    private String url;
    private String text;

    public Advertisement(int zone, int sort, String img, String url, String text) {
        setImg(img);
        setSort(sort);
        setText(text);
        setUrl(url);
        setZone(zone);
    }

    public Advertisement() {}

    public int getZone() {
        return zone;
    }

    public void setZone(int zone) {
        this.zone = zone;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
