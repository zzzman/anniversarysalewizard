package io.hbb.DAO.POJO.Premium;

import com.fasterxml.jackson.annotation.JsonInclude;

public class CreditCard {
    private int id;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String bank;
    private String creditCard;

    public CreditCard(int id, String bank, String creditCard) {
        this.id = id;
        this.bank = bank;
        this.creditCard = creditCard;
    }

    public CreditCard() { }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public String getBank() {

        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
