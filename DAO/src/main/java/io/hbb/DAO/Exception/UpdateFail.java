package io.hbb.DAO.Exception;

public class UpdateFail extends Exception{
    public UpdateFail(String message) {
        super(message);
    }
}
