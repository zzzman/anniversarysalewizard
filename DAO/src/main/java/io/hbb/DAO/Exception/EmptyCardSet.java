package io.hbb.DAO.Exception;

public class EmptyCardSet extends Exception {
    public EmptyCardSet(String message) {
        super(message);
    }
}
