package io.hbb.DAO.Exception;

public class TooManyCard extends Exception{
    public TooManyCard(String message) {
        super(message);
    }
}
