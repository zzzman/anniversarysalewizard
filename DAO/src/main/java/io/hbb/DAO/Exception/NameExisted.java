package io.hbb.DAO.Exception;

public class NameExisted extends KeyDuplicated {
    public NameExisted(String message) {
        super(message);
    }
}
