package io.hbb.DAO.Exception;

public class UsernameExisted extends KeyDuplicated {
    public UsernameExisted(String message) {
        super(message);
    }
}
