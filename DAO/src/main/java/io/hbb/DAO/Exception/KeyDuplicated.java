package io.hbb.DAO.Exception;

public class KeyDuplicated extends Exception {
    public KeyDuplicated(String message) {
        super(message);
    }
}
