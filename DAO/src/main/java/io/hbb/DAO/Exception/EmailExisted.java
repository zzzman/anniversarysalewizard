package io.hbb.DAO.Exception;

public class EmailExisted extends KeyDuplicated {
    public EmailExisted(String message) {
        super(message);
    }
}
