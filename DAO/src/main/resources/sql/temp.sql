    select p.id, p.price, p.bonus, p.bounding,d.name as department_name,
    c.begin_time, c.end_time, c.name as campaign_name, c.description, p.credit_card_id,
    dm.url, b.name as bank_name, cc.name as credit_card_name
    from premium as p, campaign as c, dm, bank as b, location as l, department as d,
    user as u, user_credit_card as uc, credit_card as cc
    where p.bank_id = uc.bank_id
    and uc.user_id = u.id
    and p.credit_card_id = cc.id
    and u.id = 1
    and p.bank_id = b.id
    and c.id = p.campaign_id
    and c.dm_id = dm.id
    and '2018-12-17' between c.begin_time and c.end_time
    and l.id = d.location_id and d.id = c.department_id
    and l.id = 1

select b.name as bank, c.name as credit_card from user_credit_card as u, bank as b, credit_card as c
where u.user_id = 1
and u.setting_number = 1
and u.bank_id = b.id
and u.credit_card_id = c.id;

select b.id as bank_id,  b.name as bank_name,
GROUP_CONCAT(CONCAT(c.id, ',',c.name) SEPARATOR '|') as credit_cards
from bank as b, credit_card as c
where b.id = c.bank_id
group by b.id;

delete from user_credit_card where setting_number = 1 and user_id = 1;
select count(id) from user_credit_card where user_id = 1 and setting_number = 1;

select id, name, password, salt, email,login_time,register_time,modify_time,is_activated from user where id = 1;

alter table user modify login_time datetime;

update user set email = ?, password = ? where id = 8;