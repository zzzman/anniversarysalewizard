 drop table if exists advertisement, user_credit_card, premium, campaign, department, department_group, location, area, credit_card, bank, mailout, callout, user;

 CREATE TABLE IF NOT EXISTS area (
     id INT not null primary key AUTO_INCREMENT,
     name varchar(20) NOT NULL,
     UNIQUE KEY(name)
 );


 CREATE TABLE IF NOT EXISTS location (
     id INT not null primary key AUTO_INCREMENT,
     name varchar(20) NOT NULL,
     area_id int NOT NULL,
     UNIQUE KEY(name),
     FOREIGN KEY(area_id) REFERENCES area(id)
 );

CREATE TABLE IF NOT EXISTS user (
     id INT not null primary key AUTO_INCREMENT,
     name char(10) NOT NULL,
     family_name varchar(5) NOT NULL,
     given_name varchar(10) NOT NULL,
     address varchar(30),
     birthday date NOT NULL,
     password varchar(512) not null,
     salt blob not null,
     email varchar(30) not null,
     login_time datetime,
     activate_time datetime,
     register_time datetime not null default CURRENT_TIMESTAMP,
     modify_time datetime not null on update CURRENT_TIMESTAMP,
     is_activated bool not null default false,
     UNIQUE KEY(name),
     UNIQUE KEY(email)
 );

 CREATE TABLE IF NOT EXISTS callout (
     id INT not null primary key AUTO_INCREMENT,
     user_id INT not null,
     phone_number char(10) not null,
     user_ip INT UNSIGNED not null,
     callout_time datetime not null default CURRENT_TIMESTAMP,
     purpose tinyint not null,
     sms char(7) not null,
     FOREIGN KEY(user_id) REFERENCES user(id)
 );

 CREATE TABLE IF NOT EXISTS mailout (
     id INT not null primary key AUTO_INCREMENT,
     user_id INT not null,
     email varchar(30) not null,
     mailout_time datetime not null default CURRENT_TIMESTAMP,
     hash_code text not null,
     FOREIGN KEY(user_id) REFERENCES user(id)
 );

 CREATE TABLE IF NOT EXISTS bank (
     id INT not null primary key AUTO_INCREMENT,
     name varchar(20) NOT NULL,
     UNIQUE KEY(name)
 );

CREATE TABLE IF NOT EXISTS department_group (
     id INT not null primary key AUTO_INCREMENT,
     name varchar(20) NOT NULL,
     UNIQUE KEY(name)
);

 CREATE TABLE IF NOT EXISTS department (
     id INT not null primary key AUTO_INCREMENT,
     department_group_id int not null,
     name varchar(20) NOT NULL,
     location_id int NOT NULL,
     url text,
     UNIQUE KEY(name),
     FOREIGN KEY (location_id) REFERENCES location(id),
     FOREIGN KEY (department_group_id) REFERENCES department_group(id)
 );

CREATE TABLE IF NOT EXISTS campaign (
     id INT not null primary key AUTO_INCREMENT,
     department_id INT not null,
     name varchar(50) NOT NULL,
     description text,
     begin_time date NOT NULL,
     end_time date NOT NULL,
     url text NOT NULL,
     FOREIGN KEY (department_id) REFERENCES department(id)
);

 CREATE TABLE IF NOT EXISTS credit_card (
     id INT not null primary key AUTO_INCREMENT,
     bank_id int not null,
     name varchar(20),
     FOREIGN KEY (bank_id) REFERENCES bank(id)
 );

  CREATE TABLE IF NOT EXISTS user_credit_card (
     id INT not null primary key AUTO_INCREMENT,
     user_id int not null,
     bank_id int not null,
     credit_card_id int not null,
     setting_number int not null,
     FOREIGN KEY (user_id) REFERENCES user(id),
     FOREIGN KEY (bank_id) REFERENCES bank(id),
     FOREIGN KEY (credit_card_id) REFERENCES credit_card(id)
 );

 CREATE TABLE IF NOT EXISTS premium (
     id INT not null primary key AUTO_INCREMENT,
     campaign_id int not null,
     bank_id int not null,
     credit_card_id int not null,
     price int NOT NULL,
     bounding int NOT NULL,
     bonus int NOT NULL,
     FOREIGN KEY (campaign_id) REFERENCES campaign(id),
     FOREIGN KEY (credit_card_id) REFERENCES credit_card(id),
     FOREIGN KEY (bank_id) REFERENCES bank(id),
     constraint chk_price check (price > 0),
     constraint chk_bounding check (bounding > 0),
     constraint chk_bonus check (bonus > 0)
 );

 CREATE TABLE IF NOT EXISTS advertisement (
     id INT not null primary key AUTO_INCREMENT,
     zone INT not null,
     sort INT not null,
     img text,
     url text,
     note text,
     create_time datetime not null default CURRENT_TIMESTAMP,
     expose_time_start datetime not null,
     expose_time_end datetime not null,
     is_activated bool not null default true,
     constraint check_time check (expose_time_end >expose_time_start  )
 );



INSERT INTO area (name) VALUES ('北部購物商城');
INSERT INTO area (name) VALUES ('中部購物商城');
INSERT INTO area (name) VALUES ('南部購物商城');
INSERT INTO area (name) VALUES ('東部購物商城');

INSERT INTO location (name, area_id) select '台北市', a.id from area as a where a.name = '北部購物商城';
INSERT INTO location (name, area_id) select '新北市', a.id from area as a where a.name = '北部購物商城';
INSERT INTO location (name, area_id) select '新竹市', a.id from area as a where a.name = '北部購物商城';
INSERT INTO location (name, area_id) select '台中市', a.id from area as a where a.name = '中部購物商城';
INSERT INTO location (name, area_id) select '桃園市', a.id from area as a where a.name = '北部購物商城';
INSERT INTO location (name, area_id) select '苗栗縣', a.id from area as a where a.name = '北部購物商城';
INSERT INTO location (name, area_id) select '嘉義市', a.id from area as a where a.name = '中部購物商城';
INSERT INTO location (name, area_id) select '台南市', a.id from area as a where a.name = '南部購物商城';
INSERT INTO location (name, area_id) select '高雄市', a.id from area as a where a.name = '南部購物商城';
INSERT INTO location (name, area_id) select '屏東縣', a.id from area as a where a.name = '南部購物商城';
INSERT INTO location (name, area_id) select '宜蘭縣', a.id from area as a where a.name = '東部購物商城';
INSERT INTO location (name, area_id) select '花蓮縣', a.id from area as a where a.name = '東部購物商城';


INSERT INTO bank (name)VALUES ('國泰世華銀行');
INSERT INTO bank (name)VALUES ('遠東國際商業銀行');
INSERT INTO bank (name)VALUES ('花旗(台灣)商業銀行');
INSERT INTO bank (name)VALUES ('玉山商業銀行');
INSERT INTO bank (name)VALUES ('台北富邦銀行');
INSERT INTO bank (name)VALUES ('永豐商業銀行');
INSERT INTO bank (name)VALUES ('彰化商業銀行');
INSERT INTO bank (name)VALUES ('台灣銀行');
INSERT INTO bank (name)VALUES ('土地銀行');
INSERT INTO bank (name)VALUES ('合作金庫商業銀行');
INSERT INTO bank (name)VALUES ('第一商業銀行');
INSERT INTO bank (name)VALUES ('華南商業銀行');
INSERT INTO bank (name)VALUES ('上海商業儲蓄銀行');
INSERT INTO bank (name)VALUES ('高雄銀行');
INSERT INTO bank (name)VALUES ('兆豐國際商業銀行');
INSERT INTO bank (name)VALUES ('台灣中小企業銀行');
INSERT INTO bank (name)VALUES ('台中商業銀行');
INSERT INTO bank (name)VALUES ('匯豐(台灣)商業銀行');
INSERT INTO bank (name)VALUES ('華泰商業銀行');
INSERT INTO bank (name)VALUES ('台灣新光銀行');
INSERT INTO bank (name)VALUES ('陽信商業銀行');
INSERT INTO bank (name)VALUES ('三信商業銀行');
INSERT INTO bank (name)VALUES ('聯邦商業銀行');
INSERT INTO bank (name)VALUES ('元大銀行');
INSERT INTO bank (name)VALUES ('凱基銀行');
INSERT INTO bank (name)VALUES ('星展銀行(台灣)');
INSERT INTO bank (name)VALUES ('台新商業銀行');
INSERT INTO bank (name)VALUES ('日盛國際商業銀行');
INSERT INTO bank (name)VALUES ('安泰商業銀行');
INSERT INTO bank (name)VALUES ('中國信託商業銀行');
INSERT INTO bank (name)VALUES ('台灣樂天信用卡股份有限公司');
INSERT INTO bank (name)VALUES ('台灣永旺信用卡股份有限公司');
INSERT INTO bank (name)VALUES ('中華郵政股份有限公司');
INSERT INTO bank (name)VALUES ('台灣美國運通國際(股)公司');

INSERT INTO credit_card(bank_id, name)
select id, '未指定' from bank;

INSERT INTO credit_card(bank_id, name)
select id, 'VIP' from bank where bank.id = 1;

INSERT INTO department_group(name)VALUES ('SOGO');
INSERT INTO department_group(name)VALUES ('新光三越');
INSERT INTO department_group(name)VALUES ('遠東百貨');
INSERT INTO department_group(name)VALUES ('微風廣場實業');
INSERT INTO department_group(name)VALUES ('吸引力生活事業');
INSERT INTO department_group(name)VALUES ('台北金融大樓公司');
INSERT INTO department_group(name)VALUES ('BELLAVITA');
INSERT INTO department_group(name)VALUES ('京華城');
INSERT INTO department_group(name)VALUES ('京站實業');
INSERT INTO department_group(name)VALUES ('環球購物中心');
INSERT INTO department_group(name)VALUES ('大葉高島屋百貨');
INSERT INTO department_group(name)VALUES ('豐東興業');
INSERT INTO department_group(name)VALUES ('大江國際');
INSERT INTO department_group(name)VALUES ('台茂商場');
INSERT INTO department_group(name)VALUES ('尚順開發');
INSERT INTO department_group(name)VALUES ('豐洋興業');
INSERT INTO department_group(name)VALUES ('中友百貨');
INSERT INTO department_group(name)VALUES ('廣三崇光國際');
INSERT INTO department_group(name)VALUES ('大魯閣新時代');
INSERT INTO department_group(name)VALUES ('劍湖山世界');
INSERT INTO department_group(name)VALUES ('南紡流通');
INSERT INTO department_group(name)VALUES ('漢神集團');
INSERT INTO department_group(name)VALUES ('大統集團');
INSERT INTO department_group(name)VALUES ('義大開發');
INSERT INTO department_group(name)VALUES ('玉星企業');
INSERT INTO department_group(name)VALUES ('統一時代');

INSERT INTO department (name, location_id, department_group_id,url) select 'SOGO天母店', l.id, d.id, 'https://www.sogo.com.tw/tm' from location as l, department_group as d where l.name = '台北市' and d.name = 'SOGO';
INSERT INTO department (name, location_id, department_group_id,url) select 'SOGO忠孝館', l.id, d.id, 'https://www.sogo.com.tw/tp1' from location as l, department_group as d where l.name = '台北市' and d.name = 'SOGO';
INSERT INTO department (name, location_id, department_group_id,url) select 'SOGO復興館', l.id, d.id, 'https://www.sogo.com.tw/tp2' from location as l, department_group as d where l.name = '台北市' and d.name = 'SOGO';
INSERT INTO department (name, location_id, department_group_id,url) select 'SOGO敦化館', l.id, d.id, 'https://www.sogo.com.tw/tp3' from location as l, department_group as d where l.name = '台北市' and d.name = 'SOGO';
INSERT INTO department (name, location_id, department_group_id,url) select 'SOGO中壢店', l.id, d.id, 'https://www.sogo.com.tw/zl' from location as l, department_group as d where l.name = '桃園市' and d.name = 'SOGO';
INSERT INTO department (name, location_id, department_group_id,url) select '遠東巨城', l.id, d.id, 'https://www.sogo.com.tw/hcbc' from location as l, department_group as d where l.name = '新竹市' and d.name = 'SOGO';
INSERT INTO department (name, location_id, department_group_id,url) select 'SOGO高雄店', l.id, d.id, 'https://www.sogo.com.tw/ks' from location as l, department_group as d where l.name = '高雄市' and d.name = 'SOGO';
INSERT INTO department (name, location_id, department_group_id,url) select '信義新天地', l.id, d.id, 'https://www.skm.com.tw/BranchPages/BranchPagesHome?UUID=4e76109e-f0db-4198-ab3e-63264964844e' from location as l, department_group as d where l.name = '台北市' and d.name = '新光三越';
INSERT INTO department (name, location_id, department_group_id,url) select '台北站前店', l.id, d.id, 'https://www.skm.com.tw/BranchPages/BranchPagesHome?UUID=9cda888a-6307-4c22-b710-664e6fd02e1e' from location as l, department_group as d where l.name = '台北市' and d.name = '新光三越';
INSERT INTO department (name, location_id, department_group_id,url) select '南西店', l.id, d.id, 'https://www.skm.com.tw/BranchPages/BranchPagesHome?UUID=84560ec6-529b-41a5-a6d6-cb32d1a68ffe' from location as l, department_group as d where l.name = '台北市' and d.name = '新光三越';
INSERT INTO department (name, location_id, department_group_id,url) select '天母店', l.id, d.id, 'https://www.skm.com.tw/BranchPages/BranchPagesHome?UUID=2ca9fead-1f4b-4f84-8d32-520c469fa5dc' from location as l, department_group as d where l.name = '台北市' and d.name = '新光三越';
INSERT INTO department (name, location_id, department_group_id,url) select '大有店', l.id, d.id, 'https://www.skm.com.tw/BranchPages/BranchPagesHome?UUID=799b72d1-df7e-4aea-aece-2fbdd4313aac' from location as l, department_group as d where l.name = '桃園市' and d.name = '新光三越';
INSERT INTO department (name, location_id, department_group_id,url) select '桃園站前店', l.id, d.id, '' from location as l, department_group as d where l.name = '桃園市' and d.name = '新光三越';
INSERT INTO department (name, location_id, department_group_id,url) select '中港店', l.id, d.id, 'https://www.skm.com.tw/BranchPages/BranchPagesHome?UUID=6c3b4600-7c00-40c6-b816-dc88a1edaac9' from location as l, department_group as d where l.name = '台中市' and d.name = '新光三越';
INSERT INTO department (name, location_id, department_group_id,url) select '垂楊店', l.id, d.id, 'https://www.skm.com.tw/BranchPages/BranchPagesHome?UUID=fd10276f-eba9-4da8-af80-2eae806728c4' from location as l, department_group as d where l.name = '嘉義市' and d.name = '新光三越';
INSERT INTO department (name, location_id, department_group_id,url) select '新天地', l.id, d.id, 'https://www.skm.com.tw/BranchPages/BranchPagesHome?UUID=03870b7b-1e3e-4264-bb1f-ea00b8b051d7' from location as l, department_group as d where l.name = '台南市' and d.name = '新光三越';
INSERT INTO department (name, location_id, department_group_id,url) select '中山店', l.id, d.id, 'https://www.skm.com.tw/BranchPages/BranchPagesHome?UUID=da03ff99-1ea4-4846-8acf-f33167d8f55d' from location as l, department_group as d where l.name = '台南市' and d.name = '新光三越';
INSERT INTO department (name, location_id, department_group_id,url) select '左營店', l.id, d.id, 'https://www.skm.com.tw/BranchPages/BranchPagesHome?UUID=0a876d6c-d804-4f8f-aa52-47c0f60eb539' from location as l, department_group as d where l.name = '高雄市' and d.name = '新光三越';
INSERT INTO department (name, location_id, department_group_id,url) select '高雄三多店', l.id, d.id, 'https://www.skm.com.tw/BranchPages/BranchPagesHome?UUID=8b9d843a-2bed-4eb8-80be-78c535deb744' from location as l, department_group as d where l.name = '高雄市' and d.name = '新光三越';
INSERT INTO department (name, location_id, department_group_id,url) select '大魯閣草衙道', l.id, d.id, 'http://www.tarokopark.com.tw/' from location as l, department_group as d where l.name = '高雄市' and d.name = '新光三越';
INSERT INTO department (name, location_id, department_group_id,url) select '遠百寶慶店', l.id, d.id, 'https://www.feds.com.tw/tw/32' from location as l, department_group as d where l.name = '台北市' and d.name = '遠東百貨';
INSERT INTO department (name, location_id, department_group_id,url) select 'FE21板橋遠百', l.id, d.id, 'https://www.feds.com.tw/tw/50' from location as l, department_group as d where l.name = '新北市' and d.name = '遠東百貨';
INSERT INTO department (name, location_id, department_group_id,url) select '板橋大遠百', l.id, d.id, 'https://www.feds.com.tw/tw/54' from location as l, department_group as d where l.name = '新北市' and d.name = '遠東百貨';
INSERT INTO department (name, location_id, department_group_id,url) select '桃園遠百', l.id, d.id, 'https://www.feds.com.tw/tw/40'  from location as l, department_group as d where l.name = '桃園市' and d.name = '遠東百貨';
INSERT INTO department (name, location_id, department_group_id,url) select '新竹大遠百', l.id, d.id, 'https://www.feds.com.tw/tw/42' from location as l, department_group as d where l.name = '新竹市' and d.name = '遠東百貨';
INSERT INTO department (name, location_id, department_group_id,url) select '台中大遠百', l.id, d.id, 'https://www.feds.com.tw/tw/53' from location as l, department_group as d where l.name = '台中市' and d.name = '遠東百貨';
INSERT INTO department (name, location_id, department_group_id,url) select '嘉義遠百', l.id, d.id, 'https://www.feds.com.tw/tw/37' from location as l, department_group as d where l.name = '嘉義市' and d.name = '遠東百貨';
INSERT INTO department (name, location_id, department_group_id,url) select '大遠百成功店', l.id, d.id, 'https://www.feds.com.tw/tw/48' from location as l, department_group as d where l.name = '台南市' and d.name = '遠東百貨';
INSERT INTO department (name, location_id, department_group_id,url) select '大遠百公園店', l.id, d.id, 'https://www.feds.com.tw/tw/34' from location as l, department_group as d where l.name = '台南市' and d.name = '遠東百貨';
INSERT INTO department (name, location_id, department_group_id,url) select '高雄大遠百', l.id, d.id, 'https://www.feds.com.tw/tw/51' from location as l, department_group as d where l.name = '高雄市' and d.name = '遠東百貨';
INSERT INTO department (name, location_id, department_group_id,url) select '花蓮遠百', l.id, d.id, 'https://www.feds.com.tw/tw/52' from location as l, department_group as d where l.name = '花蓮縣' and d.name = '遠東百貨';
INSERT INTO department (name, location_id, department_group_id,url) select '微風松高', l.id, d.id, 'https://www.breezecenter.com/branches/007' from location as l, department_group as d where l.name = '台北市' and d.name = '微風廣場實業';
INSERT INTO department (name, location_id, department_group_id,url) select '微風信義', l.id, d.id, 'https://www.breezecenter.com/branches/009' from location as l, department_group as d where l.name = '台北市' and d.name = '微風廣場實業';
INSERT INTO department (name, location_id, department_group_id,url) select '微風南山', l.id, d.id, 'https://www.breezecenter.com/branches/012' from location as l, department_group as d where l.name = '台北市' and d.name = '微風廣場實業';
INSERT INTO department (name, location_id, department_group_id,url) select '微風廣場', l.id, d.id, 'https://www.breezecenter.com/branches/001' from location as l, department_group as d where l.name = '台北市' and d.name = '微風廣場實業';
INSERT INTO department (name, location_id, department_group_id,url) select '微風南京', l.id, d.id, 'https://www.breezecenter.com/branches/005' from location as l, department_group as d where l.name = '台北市' and d.name = '微風廣場實業';
INSERT INTO department (name, location_id, department_group_id,url) select '微風台北車站', l.id, d.id, 'https://www.breezecenter.com/branches/003' from location as l, department_group as d where l.name = '台北市' and d.name = '微風廣場實業';
INSERT INTO department (name, location_id, department_group_id,url) select '京華城', l.id, d.id, 'http://web01.livingmall.com.tw/' from location as l, department_group as d where l.name = '台北市' and d.name = '京華城';
INSERT INTO department (name, location_id, department_group_id,url) select '統一時代', l.id, d.id, 'http://www.uni-ustyle.com.tw/' from location as l, department_group as d where l.name = '台北市' and d.name = '統一時代';
INSERT INTO department (name, location_id, department_group_id,url) select 'ATT 4 FUN', l.id, d.id, 'http://www.att4fun.com.tw/' from location as l, department_group as d where l.name = '台北市' and d.name = '吸引力生活事業';
INSERT INTO department (name, location_id, department_group_id,url) select '台北101', l.id, d.id, 'https://www.taipei-101.com.tw/mall.aspx' from location as l, department_group as d where l.name = '台北市' and d.name = '台北金融大樓公司';
INSERT INTO department (name, location_id, department_group_id,url) select 'BELLAVITA', l.id, d.id, 'http://www.bellavita.com.tw/cht/' from location as l, department_group as d where l.name = '台北市' and d.name = 'BELLAVITA';
INSERT INTO department (name, location_id, department_group_id,url) select '京站', l.id, d.id, 'https://www.qsquare.com.tw/' from location as l, department_group as d where l.name = '台北市' and d.name = '京站實業';
INSERT INTO department (name, location_id, department_group_id,url) select '南港車站', l.id, d.id, 'https://www.twglobalmall.com/web/global/index.html?shopid=TWG1' from location as l, department_group as d where l.name = '台北市' and d.name = '環球購物中心';
INSERT INTO department (name, location_id, department_group_id,url) select '板橋車站', l.id, d.id, 'https://www.twglobalmall.com/web/global/index.html?shopid=TWB1' from location as l, department_group as d where l.name = '新北市' and d.name = '環球購物中心';
INSERT INTO department (name, location_id, department_group_id,url) select '新北中和', l.id, d.id, 'https://www.twglobalmall.com/web/global/index.html?shopid=TWA1' from location as l, department_group as d where l.name = '新北市' and d.name = '環球購物中心';
INSERT INTO department (name, location_id, department_group_id,url) select '林口A9店', l.id, d.id, 'https://www.twglobalmall.com/web/global/index.html?shopid=TWA5' from location as l, department_group as d where l.name = '新北市' and d.name = '環球購物中心';
INSERT INTO department (name, location_id, department_group_id,url) select '桃園A8店', l.id, d.id, 'https://www.twglobalmall.com/web/global/index.html?shopid=TWA4' from location as l, department_group as d where l.name = '桃園市' and d.name = '環球購物中心';
INSERT INTO department (name, location_id, department_group_id,url) select '新左營車站店', l.id, d.id, 'https://www.twglobalmall.com/web/global/index.html?shopid=TWC1' from location as l, department_group as d where l.name = '高雄市' and d.name = '環球購物中心';
INSERT INTO department (name, location_id, department_group_id,url) select '屏東店', l.id, d.id, 'https://www.twglobalmall.com/web/global/index.html?shopid=TWA2DB' from location as l, department_group as d where l.name = '屏東縣' and d.name = '環球購物中心';
INSERT INTO department (name, location_id, department_group_id,url) select '大葉高島屋', l.id, d.id, 'http://www.dayeh-takashimaya.com.tw/' from location as l, department_group as d where l.name = '台北市' and d.name = '大葉高島屋百貨';
INSERT INTO department (name, location_id, department_group_id,url) select '比漾廣場', l.id, d.id, 'http://www.beyondplaza.com.tw/' from location as l, department_group as d where l.name = '新北市' and d.name = '豐東興業';
INSERT INTO department (name, location_id, department_group_id,url) select '大江百貨', l.id, d.id, 'https://www.metrowalk.com.tw/newweb/home.aspx' from location as l, department_group as d where l.name = '桃園市' and d.name = '大江國際';
INSERT INTO department (name, location_id, department_group_id,url) select '台茂商城', l.id, d.id, 'https://www.taimall.com.tw/' from location as l, department_group as d where l.name = '桃園市' and d.name = '台茂商場';
INSERT INTO department (name, location_id, department_group_id,url) select '尚順育樂', l.id, d.id, 'http://www.ss-plaza.com.tw/topic-area/ssmall/floor-guide/1f/' from location as l, department_group as d where l.name = '苗栗縣' and d.name = '尚順開發';
INSERT INTO department (name, location_id, department_group_id,url) select '中友百貨', l.id, d.id, 'https://www.chungyo.com.tw/' from location as l, department_group as d where l.name = '台中市' and d.name = '中友百貨';
INSERT INTO department (name, location_id, department_group_id,url) select '太平洋豐原店', l.id, d.id, 'https://fy.pacific-mall.com.tw/indexStore.php' from location as l, department_group as d where l.name = '台中市' and d.name = '豐洋興業';
INSERT INTO department (name, location_id, department_group_id,url) select '台中廣三', l.id, d.id, 'http://www.kssogo.com.tw/' from location as l, department_group as d where l.name = '台中市' and d.name = '廣三崇光國際';
INSERT INTO department (name, location_id, department_group_id,url) select '大魯閣新時代', l.id, d.id, 'http://taichung.trkmall.com.tw/' from location as l, department_group as d where l.name = '台中市' and d.name = '大魯閣新時代';
INSERT INTO department (name, location_id, department_group_id,url) select '耐斯松屋', l.id, d.id, 'http://www.niceplaza.com.tw/index.html' from location as l, department_group as d where l.name = '嘉義市' and d.name = '劍湖山世界';
INSERT INTO department (name, location_id, department_group_id,url) select '台南南紡夢時代', l.id, d.id, 'http://www.tsrd.com.tw/' from location as l, department_group as d where l.name = '台南市' and d.name = '南紡流通';
INSERT INTO department (name, location_id, department_group_id,url) select '巨蛋店', l.id, d.id, 'https://www.hanshin.com.tw/%E6%BC%A2%E7%A5%9E%E5%B7%A8%E8%9B%8B/tw' from location as l, department_group as d where l.name = '高雄市' and d.name = '漢神集團';
INSERT INTO department (name, location_id, department_group_id,url) select '大立百貨', l.id, d.id, 'http://www.starplace-talee.com.tw/' from location as l, department_group as d where l.name = '高雄市' and d.name = '大統集團';
INSERT INTO department (name, location_id, department_group_id,url) select '和平店', l.id, d.id, 'http://www.pz-peace.com.tw/' from location as l, department_group as d where l.name = '高雄市' and d.name = '大統集團';
INSERT INTO department (name, location_id, department_group_id,url) select '漢神百貨', l.id, d.id, 'https://www.hanshin.com.tw/%E6%BC%A2%E7%A5%9E%E7%99%BE%E8%B2%A8/tw' from location as l, department_group as d where l.name = '高雄市' and d.name = '漢神集團';
INSERT INTO department (name, location_id, department_group_id,url) select '義大世界', l.id, d.id, 'http://www.edamall.com.tw/BrandsGuide_byFloor.aspx' from location as l, department_group as d where l.name = '高雄市' and d.name = '義大開發';
INSERT INTO department (name, location_id, department_group_id,url) select '太平洋百貨屏東店', l.id, d.id, 'http://pd.pacific-mall.com.tw/indexStore.php' from location as l, department_group as d where l.name = '屏東縣' and d.name = '豐洋興業';
INSERT INTO department (name, location_id, department_group_id,url) select '新月廣場', l.id, d.id, 'https://www.lunaplaza.com.tw/' from location as l, department_group as d where l.name = '宜蘭縣' and d.name = '玉星企業';

INSERT INTO campaign(department_id, name, description, begin_time, end_time, url )
select d.id, '歡樂聖誕 銀行獻禮', '歡樂聖誕 銀行獻禮', '2018-12-16', '2018-12-25', 'https://www.sogo.com.tw/tp1/news/18112618145582'
from department as d
where d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館');

INSERT INTO user (name, family_name, given_name, address, birthday, password, salt, email, activate_time, login_time, register_time, modify_time, is_activated)
VALUES('0961358196' ,'王', '測試', '新北市土城區裕民路11巷22號5樓', '1990-12-31', '$2a$10$0zD2GwX5cALCjMdxfxf9f.2r/klWCrb3tZ39D/WwR5AEGmkKPbDA6', '1234', 'test@test.com','2018-12-12 00:00:00', '2018-12-12 00:00:00', '2018-12-10 00:00:00', '2018-12-10 00:00:00', true);

INSERT INTO user_credit_card (user_id, bank_id, credit_card_id, setting_number)
select 1, bank_id, id, 1 from credit_card where bank_id = 1 and name = '未指定';

INSERT INTO user_credit_card (user_id, bank_id, credit_card_id, setting_number)
select 1, bank_id, id, 1 from credit_card where bank_id = 2 and name = '未指定';

INSERT INTO user_credit_card (user_id, bank_id, credit_card_id, setting_number)
select 1, bank_id, id, 1 from credit_card where bank_id = 6 and name = '未指定';


INSERT INTO user (name, family_name, given_name, address, birthday, password, salt, email, activate_time, login_time, register_time, modify_time, is_activated)
VALUES('0911123456','林', '測試', null,  '1991-1-1','$2a$10$0zD2GwX5cALCjMdxfxf9f.2r/klWCrb3tZ39D/WwR5AEGmkKPbDA6', '1234', 'test2@test.com', '2018-12-12 00:00:00', '2018-12-12 00:00:00', '2018-12-10 00:00:00', '2018-12-10 00:00:00', true);



INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, 60, 1, 10
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館') and b.name = '國泰世華銀行' and b.id = cc.bank_id;

INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, 200, 1, 30
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館') and b.name = '國泰世華銀行' and b.id = cc.bank_id;


INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, 500, 1, 80
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館') and b.name = '國泰世華銀行' and b.id = cc.bank_id;

INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, 60, 1, 10
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館') and b.name = '遠東國際商業銀行' and b.id = cc.bank_id;

INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, 230, 1, 40
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館') and b.name = '遠東國際商業銀行' and b.id = cc.bank_id;

INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, 500, 1, 90
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館') and b.name = '遠東國際商業銀行' and b.id = cc.bank_id;

INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, 65, 1, 10
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館') and b.name = '玉山商業銀行' and b.id = cc.bank_id;

INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, 200, 1, 30
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館') and b.name = '玉山商業銀行' and b.id = cc.bank_id;

INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, 520, 1, 80
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館') and b.name = '玉山商業銀行' and b.id = cc.bank_id;

INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, 65, 1, 10
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館') and b.name = '台北富邦銀行' and b.id = cc.bank_id;

INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, 200, 1, 30
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館') and b.name = '台北富邦銀行' and b.id = cc.bank_id;

INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, 60, 1, 10
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館') and b.name = '永豐商業銀行' and b.id = cc.bank_id;

INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, 180, 1, 30
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館') and b.name = '永豐商業銀行' and b.id = cc.bank_id;

INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, 300, 1, 60
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館') and b.name = '永豐商業銀行' and b.id = cc.bank_id;

INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, 600, 1, 20
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館') and b.name = '永豐商業銀行' and b.id = cc.bank_id;

INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, 50, 1, 20
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館') and b.name = '彰化商業銀行' and b.id = cc.bank_id;

INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, 150, 1, 50
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name in ('SOGO忠孝館', 'SOGO復興館', 'SOGO敦化館') and b.name = '彰化商業銀行' and b.id = cc.bank_id;

insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(3, 1, 'https://obs.line-scdn.net/0h-c9k5CZnckwEEllNr0YNGz5EcSM3fmFPYCQjT0d8LHh8JTJKP3U_eSgae30hKjUSanw_KikRaX15cjZIaiM_/w644','https://today.line.me/tw/pc/article/2019%E4%B8%8B%E5%8D%8A%E5%B9%B4%E3%80%8C%E6%B0%A3%E5%A2%8A%E7%B2%89%E9%A4%85%E3%80%8D%E5%AE%8C%E6%95%B412%E6%AC%BE%E6%8E%A8%E8%96%A6%EF%BC%81YSL%E3%80%81%E8%BF%AA%E5%A5%A7%E3%80%81%E6%A4%8D%E6%9D%91%E7%A7%80%E3%80%81%E5%AC%8C%E8%98%AD%E3%80%81M+A+C%E3%80%81NARS%E3%80%81%E8%B3%87%E7%94%9F%E5%A0%82%E3%80%81%E9%9B%AA%E8%8A%B1%E7%A7%80%E2%80%A6%E6%9C%80%E5%BC%B7%E5%BA%95%E5%A6%9D%E4%B8%80%E6%AC%A1%E7%9C%8B-l672Zv','','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(3, 2, 'https://obs.line-scdn.net/0hu486rMa1KhsFTwEbIVVVTD8ZKXQ2IzkYYXl7GEYhdC99eGtOMSwwdSZIfX99em1FbCpteiRPMSp4L29JOCsw/w644','https://today.line.me/tw/pc/article/%E5%A4%8F%E6%9C%AB%E5%BF%85%E6%94%B6%E5%B0%8F%E6%B8%85%E6%96%B0%E9%A6%99%E6%B0%9B%E6%8E%A8%E8%96%A6%EF%BC%81TOM+FORD%E3%80%81%E6%BD%98%E6%B5%B7%E5%88%A9%E6%A0%B9%E3%80%81GUCCI%E3%80%81%E6%AD%90%E8%88%92%E4%B8%B9-LLKQrM','','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(3, 3, 'https://obs.line-scdn.net/0hSQc6HBudDH5eTyd_OWlzKWQZDxFtIx99OnldfR0hUkomeEwvMi9DEH0aUxtwK0sgMHtLH35GF08jL0ggYi5D/w644','https://today.line.me/tw/pc/article/%E6%AF%8F%E4%B8%80%E9%9A%BB%E9%83%BD%E6%98%AF%E6%96%B7%E8%B2%A8%E9%A0%90%E6%84%9F%EF%BC%812019%E4%B8%8B%E5%8D%8A%E5%B0%88%E6%AB%83%E3%80%8C%E7%8E%AB%E7%91%B0%E8%89%B2%E3%80%8D%E5%94%87%E5%BD%A9%E6%96%B0%E5%93%81%E7%86%B1%E6%90%9C%EF%BC%8C%E4%B8%80%E6%93%A6%E5%B0%B1%E6%98%AF%E6%BA%AB%E6%9F%94%E5%B0%8F%E5%A7%90%E5%A7%90-95koZe','','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(4, 1, '','https://www.youtube.com/watch?v=R2V9sHAlLuQ','','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(4, 2, '','https://www.youtube.com/watch?v=gORotxnPczg','','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(2, 1, 'https://obs.line-scdn.net/0hxAlQkDWlJ25RKAxOgyxYOWt-JAFiRDRtNR52bRJGeVopHWZtOkpqW314e1p0EGAwOE5vDnEtPF8sEWU5aktq/w644','https://today.line.me/TW/article/06Bpq2?utm_source=copyshare','白鞋強迫症的超齊全清潔大法！泛黃、髒污、帆布、皮革等不同各種材質與狀況的球鞋清潔術一次教你！','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(2, 2, 'https://obs.line-scdn.net/0hlG7JaX5DM2gKMxhpWzhMPzNlMAc5XyBrbgVia0ldbVxyBHM6ZFEuXSY3ZV8hAnQ2Y1R1CS12bAh3AH08Mwc/w644','https://today.line.me/tw/pc/article/%E7%B5%B2%E5%B7%BE%E8%B6%85%E5%BC%B75%E7%A8%AE%E7%94%A8%E6%B3%95%E5%BF%AB%E5%AD%B8%E8%B5%B7%E4%BE%86%EF%BC%81%E6%8F%9B%E5%AD%A3%E4%B8%8D%E7%94%A8%E5%86%8D%E8%B2%B7%E6%96%B0%E8%A1%A3%EF%BC%8C%E6%8F%9B%E5%80%8B%E6%89%93%E6%B3%95%E5%B0%B1%E5%83%8F%E6%8F%9B%E7%A8%AE%E9%80%A0%E5%9E%8B-0695Zp','絲巾超強5種用法快學起來！換季不用再買新衣，換個打法就像換種造型','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(2, 3, 'https://obs.line-scdn.net/0hfOHkTEtBOWB1EhJgFJxGN09EOg9GfipjESRoYzZ8Z1QNJXgxGyF-VVkQNFUMJn4-HHV0AVkbIlEIcnw-SyZ-/w644','https://today.line.me/tw/pc/article/OL+%E8%A1%A3%E6%AB%83%E5%BF%85%E5%82%99%EF%BC%81%E7%94%A8+4+%E5%80%8B%E4%B8%8A%E7%8F%AD%E6%97%8F%E5%B8%B8%E8%A6%8B%E7%9A%84%E5%A0%B4%E5%90%88%EF%BC%8C%E6%BC%94%E7%B9%B9%E5%80%8B%E4%BA%BA%E7%9F%AD%E8%A5%AF%E8%A1%AB%E7%A9%BF%E6%90%AD%E4%B8%BB%E7%BE%A9-D5KPG8','OL 衣櫃必備！用 4 個上班族常見的場合，演繹個人短襯衫穿搭主義','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(2, 4, 'https://obs.line-scdn.net/0hEr_ShLS9Gl1iKjFc6FhlClh8GTJRRgleBhxLXiFERGkaHVoCWE9RaE56FjlMGl0DDB9ROkUtAWwfSl5bCkxR/w644','https://today.line.me/tw/pc/article/%E9%9F%93%E5%9C%8B%E9%AB%AE%E5%9E%8B%E5%B8%AB%E5%88%9D%E7%A7%8B%E6%8D%B2%E5%BA%A6%E7%AF%84%E6%9C%AC%E6%8E%A8%E8%96%A6%EF%BC%81%E9%9F%93%E7%B6%B2%E5%A4%A7%E5%8B%A2%E3%80%8C%E8%8A%B1%E6%9C%B5%E7%87%99%E3%80%8D%EF%BC%8C%E7%89%B9%E6%90%9C%E4%B8%AD%E9%95%B7%E9%AB%AE%E3%80%81%E7%9F%AD%E9%AB%AE%E7%87%99%E9%AB%AE%E5%9C%96%E9%91%91-GlVe88','韓國髮型師初秋捲度範本推薦！韓網大勢「花朵燙」，特搜中長髮、短髮燙髮圖鑑','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(1, 1, 'https://photo.sogo.com.tw/Content/Upload/images/_upload/19082806384049901.jpg','https://www.sogo.com.tw/tp2/news/19081215052912','','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(1, 2, 'https://m.skm.com.tw/mSKMAdmin/WebFiles/Active/ca1ebcae-522b-475e-948f-cc385fa76cd3_20190826.jpg','https://www.skm.com.tw/EventNews/eventsDetial?ActiveUUID=f5b19335-26f4-4a50-b069-acb02a540c4e&UUID=c49931b0-92d5-4515-9434-6a55b2bea5e6','','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(1, 3, 'https://www.feds.com.tw/Uploads/Event/502f9717-696c-49ff-b002-39616a2d102b.jpg','https://www.feds.com.tw/tw/54/Event/Detail/9203','','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(1, 4, 'http://www.bellavita.com.tw/uimg/store/99_20190808006.jpg','http://www.bellavita.com.tw/catalogue/showgoods.php?d=454&n=2736','','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(1, 5, 'http://www.att4fun.com.tw/images/upload/info-top-img-jpg-1415744146.jpg','http://www.att4fun.com.tw/?d=info&k=brand&i=999c52f7-a98a-4ace-a2cc-68bd1835ebfe','','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(1, 6, 'https://breeze-assets.breeze.com.tw/www/event/6b18b53a4b83814f229aa3001a878efd0c976986.jpg?1564560191','https://www.breezecenter.com/events/204','','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(1, 7, 'https://www.qsquare.com.tw/edm/20190809/images/index_01.jpg','https://www.qsquare.com.tw/edm/20190809/index.html','','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(1, 8, 'https://www.twglobalmall.com//upload/images/4280f2d90059c4ecf67bd2ddfd9ed055.jpg','https://www.twglobalmall.com/web/global/activitydetail.html?id=1566889749630724420&shopid=TWA1','','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(1, 9, 'https://www.chungyo.com.tw/upload/image/news/sp0.jpg','https://www.chungyo.com.tw/news_detail.php?cID=1&Key=3368','','2000-1-1','2100-1-1',1);
insert into advertisement (zone,sort,img,url,note,expose_time_start,expose_time_end,is_activated) values(1, 10, 'http://www.tarokopark.com.tw/upload-images/o_1dhku03lf1va51ftcj1f1j8n1ahp7.jpg','http://www.tarokopark.com.tw/event-content.html?sn=760','','2000-1-1','2100-1-1',1);