try:
    #dev
    import jaydebeapi as dbm
except ImportError:
    #test/prod 
    import pymysql as dbm



import pandas as pd

q_checkDepName = '''select 1 from department where name = '%s';''' 
q_checkBankName = '''select 1 from bank where name = '%s';''' 
q_insertCampaign = '''INSERT INTO campaign(department_id, name, description, begin_time, end_time, url )
select d.id, '%s', '%s', '%s', '%s', '%s' from department as d where d.name = '%s'; '''
q_getLatestID = '''SELECT LAST_INSERT_ID();'''
q_insertPremium = '''INSERT INTO premium (campaign_id, bank_id, credit_card_id, price, bounding, bonus)
select c.id, b.id, cc.id, %d, %d, %d
from  campaign as c, bank as b, credit_card as cc, department as d
where c.department_id = d.id and d.name = '%s' and b.name = '%s' and b.id = cc.bank_id;'''


def getDevDBconn():
    url = 'jdbc:h2:tcp://localhost/~/test'
    user = 'sa'
    password = ''
    driver = 'org.h2.Driver'
    jar = 'c:/h2/bin/h2-1.4.199.jar'
    conn = dbm.connect(driver,url,[user,password],jar)
    return conn

def getProdDBconn():
    host = 'localhost'
    user = 'root'
    password = '1qazSE45'
    db = 'hbb'
    conn = dbm.connect(host,user,password,db)
    return conn

def getRawPremium(path):
    YMDdateparser = lambda x: pd.datetime.strptime(x, "%Y-%m-%d")

    xls = pd.ExcelFile(path)
    dm = pd.read_excel(xls, 'DM')
    campaign = pd.read_excel(xls, 'CAMPAIGN', date_parser=YMDdateparser)
    premium = pd.read_excel(xls, 'PREMIUM')
    return dm, campaign, premium


def getCampaign(path):
    dm, campaign, premium = getRawPremium(path)
    dm_campaign = pd.merge(dm,campaign,left_on=['編號'], right_on=['DM'])
    campaigns = dm_campaign[['DM', '百貨名稱', '主打活動名稱', 'DM連結', '活動名稱', '敘述', '開始時間', '結束時間' ]]
    return campaigns, premium

def dataCheck(campaigns, premiums):
    print('checking data')

    conn = getDevDBconn()
    try:
        curs = conn.cursor()
        for index, row in campaigns.iterrows():
            curs.execute(q_checkDepName % (row['百貨名稱']))
            ret = curs.fetchone()
            if(ret == None):
                raise ValueError("row %d, department name '%s' not found in db." % (index + 1,row['百貨名稱'] ))
        
        for index, row in premiums.iterrows():
            curs.execute(q_checkBankName % (row['銀行名稱']))
            ret = curs.fetchone()
            if(ret == None):
                raise ValueError("row %d, bank name '%s' not found in db." % (index + 1,row['銀行名稱'] ))

    except:
        conn.rollback()
        conn.close()
        raise
    else:
        conn.commit()
        conn.close()
        print('done')

def insertData(campaigns, premiums):
    conn = getDevDBconn()
    try:
        curs = conn.cursor()

        for index, row in campaigns.iterrows():
            print('insert %s' % (row['百貨名稱']) )
            curs.execute(q_insertCampaign % (row['活動名稱'], row['敘述'], row['開始時間'].strftime("%Y-%m-%d"), row['結束時間'].strftime("%Y-%m-%d"), row['DM連結'], row['百貨名稱'] ))
            #curs.execute(q_getLatestID)
            #db_campaign_id = curs.fetchone()

            premium = premiums[premiums['CAMPAIGN'] == row['DM']]
         
            for pindex, prow in premium.iterrows():
                print('insert premium price:%d, bonus:%d, department:%s, bank:%s' % (prow['買'], prow['送'], row['百貨名稱'], prow['銀行名稱']))
                curs.execute(q_insertPremium % (prow['買']/100, 1, prow['送']/10, row['百貨名稱'], prow['銀行名稱']))

    except:
        conn.rollback()
        conn.close()
        raise
    else:
        conn.commit()
        conn.close()




campaigns, premiums = getCampaign('./sogo.xlsx')
dataCheck(campaigns, premiums)
insertData(campaigns, premiums)
